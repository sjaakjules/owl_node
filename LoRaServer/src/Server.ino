// rf95_server.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messageing server
// with the RH_RF95 class. RH_RF95 class does not provide for addressing or
// reliability, so you should only use RH_RF95  if you do not need the higher
// level messaging abilities.
// It is designed to work with the other example rf95_client
// Tested with Anarduino MiniWirelessLoRa, Rocket Scream Mini Ultra Pro with
// the RFM95W, Adafruit Feather M0 with RFM95

//  Connections:
//  Photon RFM96
//  D2----------->DIO0
//  A2----------->NSS
//  A3----------->SCK
//  A4----------->MISO
//  A5----------->MOSI
//  GND--------->GND
//  3V3---------->3.3V IN

#include <SPI.h>
#include <RF95/RH_RF95.h>
#include "InfluxDB.h"
#include "authenticate.h"
//#include "SerialBufferRK.h"

SYSTEM_MODE(SEMI_AUTOMATIC);
STARTUP(setup_Options());

void setup_Options()
{
  WiFi.selectAntenna(ANT_AUTO);
  System.enableFeature(FEATURE_RETAINED_MEMORY);
  System.enableFeature(FEATURE_RESET_INFO);
  // System.disableUpdates();
  //  System.enableFeature(FEATURE_WIFI_POWERSAVE_CLOCK);
}
// Singleton instance of the radio driver
RH_RF95 rf95;
String WebHookName = "OwlNode_System01"; // Webhook name set within particle console.
// String ImgHookName = "Img_Upload_01";
// RH_RF95 rf95(5, 2); // Rocket Scream Mini Ultra Pro with the RFM95W
// RH_RF95 rf95(8, 3); // Adafruit Feather M0 with RFM95

InfluxDB testServer = InfluxDB("sjaak", "SystemsGarden", USERNAME, PASSWORD);

ApplicationWatchdog *wd;
SerialLogHandler logHandler;
// Need this on Arduino Zero with SerialUSB port (eg RocketScream Mini Ultra Pro)
//#define Serial SerialUSB

int hangDuration = 60000;
int updateDuration = 5000;
int listenDuration = 100;

const int queLength = 500;
String messageQue[queLength];
char codes[100][3];

long totalSent = 0;
long totalReceived = 0;

int iSending = 0;
int iReceiving = 0;

long long lastSent; // seconds since last sent.
long heartbeat = -1;

bool readyToSend;

bool inDebugMode = false;

int turnOnPin = D0; // for relay controlling wifi
int activePin = D1;
int led = D7;

int nSignals = 0; // Number of signals not sent.
String message = "No signal received";

Timer SenderTimer(updateDuration, updateSender); // In ms

Timer CheckLora(listenDuration, TryGetMessages);

void updateSender()
{
  if (Particle.connected())
  {
    readyToSend = true;
  }
}

void setup()
{
  pinMode(turnOnPin, OUTPUT);
  pinMode(led, OUTPUT);
  pinMode(activePin, INPUT);

  Serial.begin(9600);
  delay(1000);

  if (!rf95.init())
    Serial.println("init failed");
  else
  {
    rf95.setFrequency(915);
    rf95.setTxPower(23, false);
  }

  testServer.init();
  testServer.setDeviceName("SystemsGardenRelay"); // defaults to "particle"
  testServer.setDebug(true);                      // defaults to false

  // Start all callbacks.
  SenderTimer.start(); // This checks online status and updates "readyToSend"=true if connected.
  CheckLora.start();   // This trys to check LoRa, incase network drops and look fails.

  wd = new ApplicationWatchdog(hangDuration, frozenShutdownCallback, 1536);

  lastSent = Time.now();
  heartbeat = Time.now();

  /*
  for (size_t i = 0; i < queLength; i++)
  {
    messageQue[i] = '{"Yr":----,"Zo":--,"Bt":----.----,"Bp":----.----,"Ba":----.----,"Bh":----.----,"Bu":-----,"Ss":----,"Sv":----,"Tl":----.----,"Tv":----,"Ti":----,"Tf":----,"UV":----,"S1":----.----,"S2":----,"Dh":----.----,"Dd":"-------","Si": "--" ,"Mo": "--" ,"Da": "--" ,"Hr": "--" ,"--": "--" ,"Sc": "--" ,"Dv": "Server01" }';
  }
  */

  Log.info("Max lora message length is: " + String(rf95.maxMessageLength()));
  Log.info("Finished Setup loop.");
  Log.info("************************");
}

void loop()
{
  if (System.uptime() > 60 * 63)
  {
    System.reset();
  }

  Particle.connect();

  if (inDebugMode)
  {
    TryPrintMessags();
  }
  else
  {
    TrySendQue();
  }
}

/*
{"Dh":2677.5044,"S1":19.7536,"S2":0,"S3":18.4127,"S4":255,"S5":18.0258,"S6":287,"S7":19.4888,"S8":508,"Yr":2022,"Zo":10,"Dd":"Tester03","Si": "-41" ,"Mo": "4" ,"Da": "22" ,"Hr": "23" ,"Mi": "30" ,"Sc": "16" ,"Dv": "Server01" }
*/

void SendInfluxDBMessage(String msgRcived)
{

  float dh = getMessage(msgRcived, "Dh").toFloat();
  int zone = getMessage(msgRcived, "Zo").toInt();
  int year = getMessage(msgRcived, "Yr").toInt();
  String sensorName = getMessage(msgRcived, "Dd");
  String baseName = getMessage(msgRcived, "Dv");
  sensorName.replace("\"", "");

  testServer.addTag("SensorDevice", sensorName);
  // testServer.addTag("RelayDevice",baseName);

  time_t sensorTime = getTimeFromDecimal(year, zone, dh);
  long timeStamp = sensorTime - zone * 3600;
  int commaIndex = msgRcived.indexOf(',');
  bool sentDebug = false;
  int counter = 0;
  while (msgRcived.indexOf(',', commaIndex + 1) != -1)
  {
    /* there is another variable... */
    // int endIndex = message.indexOf(',', commaIndex + 1);
    String code = msgRcived.substring(commaIndex + 2, commaIndex + 4);
    // char codes[3];
    code.toCharArray(codes[counter], 3);
    // for (size_t i = 0; i < 2; i++)
    // {
    //   codes[i] = code[i];
    // }
    codes[counter][2] = '\0';
    if (code == "Yr")
    {
      break;
    }
    float value = getMessage(msgRcived, code).toFloat();
    testServer.add(codes[counter], value, timeStamp);
    commaIndex = msgRcived.indexOf(',', commaIndex + 1);
    counter++;
  }

  if (testServer.sendAll())
  { // call this to send points to InfluxDB
    Serial.println("InfluxDB updated");
  }
  else
  {
    Serial.println("InfluxDB NOT updated");
  }
}

void TrySendQue()
{
  if (iSending != iReceiving && readyToSend)
  {
    Particle.syncTime();
    waitUntil(Particle.syncTimeDone);

    Serial.println(String::format("\n(%i saved - %lu/%lu) Is = %i. Ir = %i. Last sent %is ago. Now sending: ", nSignals, totalSent + 1, totalReceived, iSending, iReceiving, Time.now() - lastSent));
    Serial.println("\t" + messageQue[iSending]);
    // Particle.publish(WebHookName, messageQue[iSending], 60, PUBLIC); // WebHook to Google Sheets
    // delay(1000);
    SendInfluxDBMessage(messageQue[iSending]);
    lastSent = Time.now();
    iSending++;
    nSignals--;
    totalSent++;

    if (iSending >= queLength)
    {
      iSending = 0;
    }
    readyToSend = false;
  }
  else
  {
    if (Time.now() - heartbeat > 1)
    {
      heartbeat = Time.now();
      // Serial.print("*");
      wd->checkin();
    }
  }
}

void frozenShutdownCallback()
{
  Log.info("Device is frozen. Forced shutdown...");
  System.reset();
  delay(1000);
  System.reset(RESET_NO_WAIT);
}

void TryPrintMessags()
{

  if (rf95.available())
  {

    // Should be a message for us now
    uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    if (rf95.recv(buf, &len))
    {
      digitalWrite(led, HIGH);

      Log.info("got request: ");
      Log.info((char *)buf);
      String newMsg = String((char *)buf);
    }
    else
    {
      Serial.println("recv failed");
    }
  }
}

void TryGetMessages()
{
  if (rf95.available())
  {
    // Should be a message for us now
    uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    if (rf95.recv(buf, &len))
    {
      digitalWrite(led, HIGH);

      String newMsg = String((char *)buf);
      Serial.println(String::format("\n(%i saved - %lu/%lu) Is = %i. Ir = %i. New request: ", nSignals, totalSent, totalReceived + 1, iSending, iReceiving));
      Serial.println("\t" + newMsg);

      int msgStart = newMsg.indexOf("{");
      int msgEnd = newMsg.indexOf("}");
      if (msgStart > 0 && msgEnd > 0)
      {
        String sender = newMsg.substring(0, msgStart);

        String msgRcived = newMsg.substring(msgStart, msgEnd + 1);

        if (sender.length() > 0 && msgRcived.length() > 0)
        {
          msgRcived.replace("Dv", "Dd");
          msgRcived.remove(msgRcived.length() - 1);
          msgRcived.concat(",\"Si\": \"" + String(rf95.lastRssi()) + "\" ");

          float dh = getMessage(msgRcived, "Dh").toFloat();
          int zone = getMessage(msgRcived, "Zo").toInt();
          int year = getMessage(msgRcived, "Yr").toInt();

          time_t sensorTime = getTimeFromDecimal(year, zone, dh);

          msgRcived.concat(",\"Mo\": \"" + String(Time.month(sensorTime)) + "\" ");
          msgRcived.concat(",\"Da\": \"" + String(Time.day(sensorTime)) + "\" ");
          msgRcived.concat(",\"Hr\": \"" + String(Time.hour(sensorTime)) + "\" ");
          msgRcived.concat(",\"Mi\": \"" + String(Time.minute(sensorTime)) + "\" ");
          msgRcived.concat(",\"Sc\": \"" + String(Time.second(sensorTime)) + "\" ");

          msgRcived.concat(",\"Dv\": \"Server01\" }");
          messageQue[iReceiving] = String(msgRcived);

          nSignals += 1;
          totalReceived++;

          iReceiving++;
          if (iReceiving >= queLength)
          {
            iReceiving = 0;
          }
          // Send a reply

          sender.concat(' ');
          char dataMsg[sender.length() + 1];
          sender.toCharArray(dataMsg, sender.length());

          uint8_t data[sender.length() + 1];
          for (size_t i = 0; i < sender.length() + 1; i++)
          {
            data[i] = uint8_t(dataMsg[i]);
          }

          rf95.send(data, sizeof(data));
          rf95.waitPacketSent();
        }
        else
        {
          Serial.println("ID or message not found. No reply message sent.");
        }
      }
      else
      {
        Serial.println("No start or end bracket found, no reply message sent.");
      }
      digitalWrite(led, LOW);
    }
    else
    {
      Serial.println("recv failed");
    }
  }
  else
  {
    if (Time.now() - heartbeat > 1)
    {
      // heartbeat = Time.now();
      // Serial.print(".");
    }
    // Serial.print(".");
  }
}

String getMessage(String msgRcived, String item)
{

  int dh_start = msgRcived.indexOf(item);
  int dh_Fin = msgRcived.indexOf(",", dh_start);
  String dh_string = msgRcived.substring(dh_start + 4, dh_Fin);
  return dh_string;
}

time_t getTimeFromDecimal(float Dh_time)
{
  intmax_t secEpoc_Now = Time.now();
  float DhYr_Now = getDecimalHr(secEpoc_Now);
  intmax_t secYr_Now = DhYr_Now * 3600;
  intmax_t secYr_time = Dh_time * 3600;
  intmax_t secEPOC_time = secEpoc_Now - secYr_Now + secYr_time;
  time_t timeOut = secEPOC_time;

  return timeOut;
}

time_t getTimeFromDecimal(int year, int zone, float Dh_time)
{
  int additionalSeconds = 0;
  if (year > 2020)
  {
    additionalSeconds = floor((year - 2021) / 4 + 1) * 86400;
  }
  long secStartOfYear = 1546300800 + (year - 2019) * 31536000 + additionalSeconds;
  long secDhtime = Dh_time * 3600 + zone * 3600;
  // long diffFromSoY = secFromEpoc - secStartOfYear;

  intmax_t secEpoc_Now = Time.now();
  float DhYr_Now = getDecimalHr(secEpoc_Now);
  intmax_t secYr_Now = DhYr_Now * 3600;
  intmax_t secYr_time = Dh_time * 3600;
  // intmax_t secEPOC_time = secEpoc_Now - secYr_Now + secYr_time;

  intmax_t secEPOC_time = secDhtime + secStartOfYear;
  time_t timeOut = secEPOC_time;

  return timeOut;
}

float getDecimalHr()
{
  // Log.info("Getting decimal hours for now.");
  int year = Time.year();
  long secFromEpoc = Time.now();
  int additionalSeconds = 0;
  if (year > 2020)
  {
    additionalSeconds = floor((year - 2021) / 4 + 1) * 86400;
  }
  long secStartOfYear = 1546300800 + (year - 2019) * 31536000 + additionalSeconds;
  long diffFromSoY = secFromEpoc - secStartOfYear;
  float hrs = diffFromSoY * 1.0 / 3600.0;

  return hrs;
}

float getDecimalHr(time_t timeIn)
{
  // Log.info("Calculating Dh from %d seconds", (intmax_t)time);
  int year = Time.year(timeIn);
  long secFromEpoc = (intmax_t)timeIn;
  int additionalSeconds = 0;
  if (year > 2020)
  {
    additionalSeconds = floor((year - 2021) / 4 + 1) * 86400;
  }
  long secStartOfYear = 1546300800 + (year - 2019) * 31536000 + additionalSeconds;
  // Log.info("Seconds for the start of year: " + String(secStartOfYear));
  long diffFromSoY = secFromEpoc - secStartOfYear;
  float hrs = diffFromSoY * 1.0 / 3600.0;
  return hrs;
}
