//=======================================
//  dependency guards
#ifndef SensorController_h
#define SensorController_h

#if defined(PARTICLE)
#include <Particle.h>
#if (SYSTEM_VERSION < 0x00060100)
#error requires system target 0.6.1 or above
#endif
#endif

#if defined(PARTICLE)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#if !defined(PARTICLE)
#include "Wire.h"
#endif

//======================================
//  Forward declared dependencies
class SensorBase;
class BME280;
class VEML6070;

//======================================
//  Dependencies
#include "SensorBase.h"
#include <vector>
#include <string>

// using namespace std;

// Sensors in use
#include "Sensors/VEML6070.h"
#include "Sensors/BME280.h"
#include "Sensors/TSL2561.h"
#include "Sensors/CCS811.h"
#include "Sensors/MCP9808_j.h"
#include "Sensors/HTU21.h"
#include "Sensors/ads1115_1.h"
#include "Sensors/ads1115_2.h"
#include "Sensors/ads1115_3.h"
#include "Sensors/ads1115_4.h"
#include "Sensors/SGP30.h"
#include "Sensors/SOIL1.h"
#include "Sensors/SOIL2.h"
#include "Sensors/SOIL3.h"
#include "Sensors/SOIL4.h"

//======================================
//  Defined variables

// The index for JSON key for each sensor reading.

#define MCP9808_addr 0x18 // Temperature
#define HTU21D_addr 0x40  // humidity
#define MS5637_addr 0x76  // Pressure -- Conflict!
#define CCS811_addr 0x5B  // VOCs CO2

#define BME280_addr 0x76 // Temperature, Pressure, Humidity (0x76 SO to gnd)
#define SGP30_addr 0x58  // VOCs CO2

#define VEML6070_1_addr 0x38 // UV light
#define VEML6070_2_addr 0x39 // UV light
#define TSL2561_addr 0x29    // IR and visual light
#define DS1307_1_addr 0x50   // RTC
#define DS1307_2_addr 0x68   // RTC

#define ADS1115_1_addr 0x48 // analogue expander
#define ADS1115_2_addr 0x49 // analogue expander
#define ADS1115_3_addr 0x4A // analogue expander
#define ADS1115_4_addr 0x4B // analogue expander
//#define SGP30_addr 0x38      //Vocs Co2
#define SOIL_1_addr 0x36 // 0x38   // SeeSaw capacitive sensor 1
#define SOIL_2_addr 0x37 // 0x39   // SeeSaw capacitive sensor 1
#define SOIL_3_addr 0x38 // 0x39   // SeeSaw capacitive sensor 1
#define SOIL_4_addr 0x39 // 0x39   // SeeSaw capacitive sensor 1

// https://learn.adafruit.com/adafruit-tca9548a-1-to-8-i2c-multiplexer-breakout/wiring-and-test
#define I2CMUX1 0x70 // i2C Mux TCA9548A
#define I2CMUX2 0x71 // i2C Mux TCA9548A
#define I2CMUX3 0x72 // i2C Mux TCA9548A
#define I2CMUX4 0x73 // i2C Mux TCA9548A
#define I2CMUX5 0x74 // i2C Mux TCA9548A
#define I2CMUX6 0x75 // i2C Mux TCA9548A
#define I2CMUX7 0x76 // i2C Mux TCA9548A --Conflict!
#define I2CMUX8 0x77 // i2C Mux TCA9548A

#define null_index 0
#define Dh_Index 8
#define Yr_Index 1
#define Zo_Index 7
#define nCodesTotal 51
#define sensorsZome 10

//======================================
// The Class
class SensorController
{

public:
  SensorController(void){}; // Start controller and initalise std values
  bool hasGas() { return gasSensor.isConnected(); };
  void SetupSensors(); // Setup sensors. update connections.
  bool setGasBase(uint16_t eCO2_baseline, uint16_t TVOC_baseline);
  uint16_t *getGasBase();
  // bool checkCalibration(float relHumid, float temp, u_int32_t updateFreq);
  void UpdateSensors();
  // void CommunicateSensors();
  // void UploadSensors();
  // uint8_t getNumberOfReadings() { return totalReadings; }; // sensorReadings.size(); };
  uint8_t getLatestReadingsCount() { return latestReadingCodes.size(); };
  uint8_t *getLatestReadingCodes() { return latestReadingCodes.data(); };
  float *getLatestReadings() { return latestReadings.data(); };
  int getReadingCounter() { return readingCounter; };
  // float *getLatestData(){};                                //{ return staticReadingsVec[savedReadings - 1].data(); };
  // String getJsonCodes(int index){};                        //{ return String(JasonNameVec[index]); };

  void sleep(){}; //{ weatherSensor.goToSleep(); };

  void Calibrate()
  {
    if (gasSensor.isConnected())
    {
      weatherSensor.UpdateReading();
      gasSensor.setAbsHumidity(weatherSensor.getAbsHumidity());
    }
  };

  bool isReadyToSend() { return readyToSend; };
  String ComName;
  char JSONnames[nCodesTotal][3]{"--", "Yr", "Mo", "Da", "Hr", "Mi", "Sc", "Zo", "Dh", // 0|8 : Time
                                 "01", "02", "03", "04",                               // 8|4: ADS1115
                                 "05", "06", "07", "08",                               // 12|4: ADS1115
                                 "09", "10", "11", "12",                               // 16|4: ADS1115
                                 "13", "14", "15", "16",                               // 20|4: ADS1115
                                 "Bt", "Bp", "Ba", "Bh", "Bu",                         // 24|5: BME280
                                 "Cc", "Cv", "Ct",                                     // 29|3 : CCS811
                                 "Ht", "Hh",                                           // 32| 2 : HTU21
                                 "Mt",                                                 // 34| 1 : MCP9808
                                 "Ss", "Sv",                                           // 35|2 : SGP30
                                 "S1", "S2",                                           // 37|2 : Soil
                                 "S3", "S4",                                           // 39|2 : Soil
                                 "S5", "S6",                                           // 41|2 : Soil
                                 "S7", "S8",                                           // 43|2 : Soil
                                 "Tl", "Tv", "Ti", "Tf",                               // 45|4 : TSL2561
                                 "UV"};                                                // 49|1 : VEML6070

private:
  void calculateLatestReadigns();
  std::vector<SensorBase *> sensorsConnected;
  std::vector<SensorBase *> errorSensors;

  //  8 readings for time
  ads1115_1 analogueSensors1 = ads1115_1(9, JSONnames);  //  4 readings
  ads1115_2 analogueSensors2 = ads1115_2(13, JSONnames); //  4 readings
  ads1115_3 analogueSensors3 = ads1115_3(17, JSONnames); //  4 readings
  ads1115_4 analogueSensors4 = ads1115_4(21, JSONnames); //  4 readings
  BME280 weatherSensor = BME280(25, JSONnames);          //  5 readings  // 0x77 //Temperature, Pressure, Humidity (0x76 SO to gnd)
  CCS811 gasSensor2 = CCS811(30, JSONnames);             //  3 readings  // CCS811_addr 0x5B  //VOCs CO2
  HTU21 humiditySensor = HTU21(33, JSONnames);           //  2 readings  // 0x40  //humidity
  MCP9808_j tempSensor = MCP9808_j(35, JSONnames);       //  1 reading   // 0x18 //Temperature
  SGP30 gasSensor = SGP30(36, JSONnames);                // 2 readings   // 0x58       //VOCs CO2
  SOIL1 soilSensor1 = SOIL1(38, JSONnames);              // 2 readings
  SOIL2 soilSensor2 = SOIL2(40, JSONnames);              // 2 readings
  SOIL3 soilSensor3 = SOIL3(42, JSONnames);              // 2 readings
  SOIL4 soilSensor4 = SOIL4(44, JSONnames);              // 2 readings
  TSL2561 luxSensor = TSL2561(46, JSONnames);            //  4 readings
  VEML6070 uvSensor = VEML6070(50, JSONnames);           // 1 reading

  const static uint8_t maxSavedReadings = 10; // N readings to take median from

  std::vector<float> latestReadings;
  std::vector<uint8_t> latestReadingCodes;

  float rawReadings[nCodesTotal][maxSavedReadings];
  String rawReadingsCodes[nCodesTotal];

  int totalReadings;     // This is number of readings for this sensor event
  int savedReadings = 0; // This is the number of times sensor event has taken readings.
  bool readyToSend = false;
  int readingCounter = 0;
};

#endif
