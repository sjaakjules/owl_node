//=======================================
//  dependency guards
#ifndef SensorBase_h
#define SensorBase_h

#if defined(PARTICLE)
#include <Particle.h>
#if (SYSTEM_VERSION < 0x00060100)
#error requires system target 0.6.1 or above
#endif
#endif

#if (ARDUINO >= 100) || defined(PARTICLE)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#if !defined(PARTICLE)
#include "Wire.h"
#endif

//======================================
//  Forward declared dependencies

//======================================
//  Dependencies
#include "RunningAverage.h"
#include <vector>
#include <string>

// using namespace std;

//======================================
// The Class
class SensorBase
{

protected:
  SensorBase(uint8_t nValues, int JSONStart, char JSONList[][3], String *JSONCodes)
  {
    _nSensorReadings = nValues;

    _JSONIndex = JSONStart;

    Serial.print("Sending with flag(s): ");
    for (uint8_t j = 0; j < nValues; j++)
    {
      JSONCodes[j] = String(JSONList[j + JSONStart]);
      Serial.print(JSONCodes[j] + " , ");
    }
  };
  String _name;
  int _nSensorReadings;
  bool _hasSetup = false;
  int _JSONIndex;
  int sensorID;

public:
  virtual bool Connect(uint8_t addr) = 0;
  virtual bool UpdateReading() = 0;
  virtual void PrintToSerial() = 0;
  virtual float *getLatestValues() = 0;
  int getCodeIndexStart() { return _JSONIndex; };
  bool isConnected() { return _hasSetup; };
  String getName() { return _name; };
  int getNumberSensors() { return _nSensorReadings; };
};

#endif
