/*
 * Project SensorNode
 * Description: Main script
 * Author:  Julian Rutten - jrutten@swin.edu.au
 * Date:    11/07/2018
 */

//=======================================
//  dependency guards
#if defined(PARTICLE)
#include <Particle.h>
#if (SYSTEM_VERSION < 0x00060100)
#error requires system target 0.6.1 or above
#endif
#endif

#if (defined(ARDUINO) && (ARDUINO >= 100)) || defined(PARTICLE)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#if !defined(PARTICLE)
#include "Wire.h"
#endif

//======================================
//  Dependencies

#include "Sensors/SOIL1.h"
#include "SensorBase.h"
#include "SensorController.h"
#include <vector>
#include <string>
#include <math.h>

// using namespace std;

SOIL1::SOIL1(int i, char JSONList[][3]) : SensorBase(nReadings, i, JSONList, JSONCodes)
{
  _name = "AdaFruit Soil Sensor 1";
  _JSONIndex = i;
  //_nSensorReadings = 4;
  Serial.printlnf("for " + _name);
}

bool SOIL1::Connect(uint8_t addr)
{
  // Only one sensor needs to work for the conneciton to be active.
  if (!_hasSetup)
  {
    if (!ss.begin(0x36))
    {
      Serial.println("ERROR! Soil sensor 1 not found");
    }
    else
    {
      Serial.print("Soil sensor 1 started! version: ");
      Serial.println(ss.getVersion(), HEX);
      _hasSetup = true;
      delay(10);
      UpdateReading();
    }
  }
  /*
    if (!_hasSetup) {
      ss1.begin(VEML6070_4_T);

        Serial.println("VEML6070 connected!");
        _hasSetup = true;
        latestreading[0] =ss1.readUV();

    }*/
  return _hasSetup;
}

bool SOIL1::UpdateReading()
{
  latestreading[0] = ss.getTemp();
  latestreading[1] = ss.touchRead(0);
  return true;
}

void SOIL1::PrintToSerial()
{
  Serial.println(_name);

  Serial.print("Soil sensor 1 = ");
  Serial.print(latestreading[0]);
  Serial.print(" degrees. : Capacitance:");
  Serial.print(latestreading[1]);
  Serial.println(" unitless");
  Serial.println();
}

/*
    ===================================
      notes and info

      // The SeeSaw soil sensors are Capacitance based. They have 4 addresses:
      #define SOIL_ADDR_0 0x36
      #define SOIL_ADDR_1 0x37
      #define SOIL_ADDR_2 0x38
      #define SOIL_ADDR_3 0x39


    Functions
    getTemp();
    touchRead(0);

    */
