/*
 * Project SensorNode
 * Description: Main script
 * Author:  Julian Rutten - jrutten@swin.edu.au
 * Date:    11/07/2018
 */

//=======================================
//  dependency guards
#if defined(PARTICLE)
#include <Particle.h>
#if (SYSTEM_VERSION < 0x00060100)
#error requires system target 0.6.1 or above
#endif
#endif

#if (defined(ARDUINO) && (ARDUINO >= 100)) || defined(PARTICLE)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#if !defined(PARTICLE)
#include "Wire.h"
#endif

//======================================
//  Dependencies

#include "Sensors/SOIL4.h"
#include "SensorBase.h"
#include "SensorController.h"
#include <vector>
#include <string>
#include <math.h>

// using namespace std;

SOIL4::SOIL4(int i, char JSONList[][3]) : SensorBase(nReadings, i, JSONList, JSONCodes)
{
  _name = "AdaFruit Soil Sensor 4";
  _JSONIndex = i;
  //_nSensorReadings = 4;
  Serial.printlnf("for " + _name);
}

bool SOIL4::Connect(uint8_t addr)
{
  // Only one sensor needs to work for the conneciton to be active.
  if (!_hasSetup)
  {

    if (!ss.begin(0x39))
    {
      Serial.println("ERROR! Soil sensor 4 not found");
    }
    else
    {
      Serial.print("Soil sensor 4 started! version: ");
      Serial.println(ss.getVersion(), HEX);
      _hasSetup = true;
      delay(10);
      UpdateReading();
    }
  }
  return _hasSetup;
}

bool SOIL4::UpdateReading()
{
  latestreading[0] = ss.getTemp();
  latestreading[1] = ss.touchRead(0);
  return true;
}

void SOIL4::PrintToSerial()
{
  Serial.println(_name);

  Serial.print("Soil sensor 4 = ");
  Serial.print(latestreading[0]);
  Serial.print(" degrees. : Capacitance:");
  Serial.print(latestreading[1]);
  Serial.println(" unitless");
  Serial.println();
}

/*
    ===================================
      notes and info

      // The SeeSaw soil sensors are Capacitance based. They have 4 addresses:
      #define SOIL_ADDR_0 0x36
      #define SOIL_ADDR_1 0x37
      #define SOIL_ADDR_2 0x38
      #define SOIL_ADDR_3 0x39


    Functions
    getTemp();
    touchRead(0);

    */
