/*
 * Project SensorNode
 * Description: Main script
 * Author:  Julian Rutten - jrutten@swin.edu.au
 * Date:    11/07/2018
 */

//=======================================
//  dependency guards
#if defined(PARTICLE)
#include <Particle.h>
#if (SYSTEM_VERSION < 0x00060100)
#error requires system target 0.6.1 or above
#endif
#endif

#if (defined(ARDUINO) && (ARDUINO >= 100)) || defined(PARTICLE)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#if !defined(PARTICLE)
#include "Wire.h"
#endif

//======================================
//  Dependencies

#include "Sensors/SGP30.h"
#include "SensorBase.h"
#include "SensorController.h"
#include <vector>
#include <string>

// using namespace std;

SGP30::SGP30(int i, char JSONList[][3]) : SensorBase(nReadings, i, JSONList, JSONCodes)
{
  _name = "SGP30 VOCs/eCO2 gas Sensor";
  _JSONIndex = i;
  //  _nSensorReadings = 3;
  Serial.printlnf("for " + _name);
}

bool SGP30::Connect(uint8_t addr)
{
  if (!_hasSetup)
  {
    if (!sgp.begin())
    {
      Serial.println("Could not find a valid SGP30 sensor, check wiring!");
      _hasSetup = false;
    }
    else
    {
      if (!sgp.IAQinit())
      {
        Log.error("failed to initialize sensor.");
        _hasSetup = false;
      }
      else
      {
        // Log.info("Sensor is initialized. Getting Base Values.");
        uint16_t TVOC_base, eCO2_base;
        if (!sgp.getIAQBaseline(&eCO2_base, &TVOC_base))
        {
          Log.error("Failed to get baseline readings");
        }
        else
        {
          // Log.info("Sensor Chip base values: \teCO2: " + String(eCO2_base) + " \tTvoc: " + String(TVOC_base));
          baseValues[0] = eCO2_base;
          baseValues[1] = TVOC_base;
        }
        // secondsSinceBaseLineUpdate = Time.now();
        Serial.println("SGP30 Connected!");
        _hasSetup = true;
      }
    }
  }

  return _hasSetup;
}

void SGP30::setAbsHumidity(u_int32_t absoluteHumidityScaled)
{

  sgp.setHumidity(absoluteHumidityScaled);
}

bool SGP30::setBaseLine(uint16_t eCO2_baseline, uint16_t TVOC_baseline)
{
  uint16_t TVOC_base, eCO2_base;
  if (!sgp.getIAQBaseline(&eCO2_base, &TVOC_base))
  {
    Log.error("Failed to get baseline readings");
  }
  else
  { // read base line values
    if (TVOC_base != baseValues[1] || eCO2_base != baseValues[0])
    {
      Log.info("Read base values are different from software. Updating software values...");
      baseValues[1] = TVOC_base;
      baseValues[0] = eCO2_base;
    }
    Log.info("Sensor Chip base values: \teCO2: " + String(eCO2_base) + " \tTvoc: " + String(TVOC_base));
  }
  if (!sgp.setIAQBaseline(eCO2_baseline, TVOC_baseline))
  {
    Serial.println("Failed to set new baseline readings");
    return false;
  }
  Log.info("New base values: \teCO2: " + String(eCO2_baseline) + " \tTvoc: " + String(TVOC_baseline));
  baseValues[1] = TVOC_baseline;
  baseValues[0] = eCO2_baseline;
  return true;
}

uint16_t *SGP30::returnBaseLine()
{
  uint16_t TVOC_base, eCO2_base;
  if (!sgp.getIAQBaseline(&eCO2_base, &TVOC_base))
  {
    Serial.println("Failed to get baseline readings");
    return baseValues;
  }
  baseValues[0] = eCO2_base;
  baseValues[1] = TVOC_base;
  return baseValues;
}

bool SGP30::UpdateReading()
{
  if (!sgp.IAQmeasure())
  {
    return false;
  }
  latestreading[0] = sgp.eCO2;
  latestreading[1] = sgp.TVOC;

  return true;
}

void SGP30::PrintToSerial()
{
  Serial.println(_name);

  Serial.print("Equivalent CO2 = ");
  Serial.print(latestreading[0]);
  Serial.println(" ppm");

  Serial.print("Total VOCs = ");
  Serial.print(latestreading[1]);
  Serial.println(" ppb");

  Serial.println();
}

/*
===================================
notes and info

// I2C address options

// the i2c address
#define SGP30_I2CADDR_DEFAULT 0x58     ///< SGP30 has only one I2C address


Functions


boolean begin(TwoWire *theWire = NULL);
boolean IAQinit(void);
boolean IAQmeasure(void);

boolean getIAQBaseline(uint16_t *eco2_base, uint16_t *tvoc_base);
boolean setIAQBaseline(uint16_t eco2_base, uint16_t tvoc_base);

The last measurement of the IAQ-calculated Total Volatile Organic Compounds in ppb. This value is set when you call {@link IAQmeasure()}
uint16_t TVOC;


The last measurement of the IAQ-calculated equivalent CO2 in ppm. This value is set when you call {@link IAQmeasure()}
uint16_t eCO2;




*/
