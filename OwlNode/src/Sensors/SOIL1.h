//=======================================
//  dependency guards
#ifndef SOIL1_h
#define SOIL1_h

#if defined(PARTICLE)
#include <Particle.h>
#if (SYSTEM_VERSION < 0x00060100)
#error requires system target 0.6.1 or above
#endif
#endif

#if (ARDUINO >= 100) || defined(PARTICLE)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#if !defined(PARTICLE)
#include "Wire.h"
#endif

//======================================
//  Forward declared dependencies

//======================================
//  Dependencies
#include <vector>
#include <string>
#include "SensorBase.h"

#include "Adafruit_Seesaw.h"

// using namespace std;

//======================================
// The Class
class SOIL1 : public SensorBase
{
public:
  SOIL1(int i, char JSONList[][3]);

  bool Connect(uint8_t addr);
  bool UpdateReading();
  void PrintToSerial();
  float *getLatestValues() { return latestreading; };
  String *getJSONCodes() { return JSONCodes; };

  String getJSONData()
  {
    String data;
    for (size_t i = 0; i < nReadings; i++)
    {
      data.concat(String("\"" + JSONCodes[i] + "\":" + String(latestreading[i], 4) + ", "));
    }
    return data;
  };

private:
  const static int nReadings = 2;
  Adafruit_Seesaw ss;
  float latestreading[nReadings] = {1.0, 1.0};
  String JSONCodes[nReadings]; // = {String("S1"),String("S2")};
};

#endif
