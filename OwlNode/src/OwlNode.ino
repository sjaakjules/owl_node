/*
 * Project OwlNode
 * Description:   This project is the sensor controller on Particle Photon devices.
 *                This has a number of sensors which is defined in the Sensor controller.
 * Author:        Julian Rutten
 * Contact        julianrutten@outlook.com
 * Date:          01/01/2020
 *
 * TODO:       1) Soil moisture sensor is noisy. Requires averaging over 10 readings.
 *             3) JSONcode is populated on wakeup. If WiFi enabled loop doesn't connect all sensors signals will be incorrectly tagged.
 */

#include "SensorController.h"
#include "SensorBase.h"

#include "SPI.h"
#include "SdFat.h"
#include <list>
//#include <iostream>
//#include <algorithm>

#include "RF95/RH_RF95.h"
// using namespace std;

SYSTEM_MODE(SEMI_AUTOMATIC);
STARTUP(setup_Options());

//////////////////////////////////////////////////////////////
//            STRUCTS AND STARTUP
//////////////////////////////////////////////////////////////
struct EEPROMinfo
{
  uint8_t flag;
  char name[24];
  uint16_t TVOC_base, eCO2_base;
  bool setGas;
  long long gasSetDate;

  EEPROMinfo(String newname) : flag(0), name(""), TVOC_base(0), eCO2_base(0), setGas(false), gasSetDate(0)
  {
    if (newname.length() < 24)
    {
      for (size_t i = 0; i < newname.length(); i++)
      {
        name[i] = newname.charAt(i);
      }
    }
    else
    {
      for (size_t i = 0; i < 24; i++)
      {
        name[i] = newname.charAt(i);
      }
    }
  }

  EEPROMinfo() : flag(0), TVOC_base(0), eCO2_base(0), setGas(false), gasSetDate(0)
  {
    name[0] = '\0';
  }
};

ApplicationWatchdog *wd;

void setup_Options()
{
  WiFi.selectAntenna(ANT_AUTO);
  System.enableFeature(FEATURE_RETAINED_MEMORY);
  System.enableFeature(FEATURE_RESET_INFO);
  System.disableUpdates();
  // System.enableFeature(FEATURE_WIFI_POWERSAVE_CLOCK);
}

//////////////////////////////////////////////////////////////
//           DEBUG VARIABLES
//////////////////////////////////////////////////////////////
bool refreshSavedWifi = false; // Change to true to clear wifi credentials. Will need to re-flash with false!
bool isDebugMode = false;      // Forces to wait for serial connection.

//////////////////////////////////////////////////////////////
//            DEPLOYMENT SETTINGS (CHANGE TO SUIT)
//////////////////////////////////////////////////////////////
bool sendLoopedViaWifi = false;
bool sendLoopedViaLoRa = true;
bool isTestingPower = false;
bool isControllingRaspPi = false;
bool isPausingPIR = false;
// const int maxOfflineSaves = 30;                       // Sets size of offline saves. Will loop if exceeded.
const int PIR1Size = 1;                               // Sets size of PIR saves between uploads. Will loop if exceeded.
const int PIR2Size = 1;                               // Sets size of PIR saves between uploads. Will loop if exceeded.
const int nSensorLoopsPerSave = 1;                    // For online, so if =6, every 6th sensor loop will push to the cloud. ()=0 for no wifi.)
const float interval = 3;                             // interval in minutes to wake with first one at O'clock.
const long long calibrationFrequency = 7 * 24 * 3600; // (seconds) Need to update base values every 5 days or so.
const uint32_t calibrationDuration = 10;              // 3600;                                                 // 3600;        // (seconds) number of seconds for calibration.
const uint32_t FirstCalibration = 30;                 // 12*3600;             //12*3600;                 // (seconds) number of seconds for first calibration.
int firstLoadDelay = FirstCalibration + 30;           // 3*3600;           //12*3600;        // Seconds of the first startup. If initial setup then install in tree it might be several hours!

//////////////////////////////////////////////////////////////
//           SYSTEM VARIABLES
//////////////////////////////////////////////////////////////
//     WILL BE DIFFERENT FOR YOUR SYSTEM!!
//////////////////////////////////////////////////////////////
u_int16_t PIR1Pin = WKP; // D5; // can turn it off by setting it to WKP.
u_int16_t PIR2Pin = WKP; // D6; // can turn it off by setting it to WKP.
u_int16_t sensorPin = D3;
u_int16_t WifiPin = D4;
u_int16_t powerPIn = D6; // D7; //D6;
u_int16_t raspPiPinOut = D7;
u_int16_t raspPiPinIn = D6;

bool sendViaLoRaModule = true;  // Make true if it has a RF95 lora module connected.
bool sendViaWifiModule = false; // Make true if there is a wifi module with power trigger.
bool hasSensors = true;
bool hasPIR = false;
// const uint16_t nSaves = 1;                 // Does not work yet... keep as 1.
String WebHookName = "OwlNode_System01";   // Webhook name set within particle console.
const uint16_t maxPIRCountPerMessage = 19; // 19 should be ok, 20 will fail if name is large or many messages sent at once...

//////////////////////////////////////////////////////////////
//           SYSTEM VARIABLES (FIXED)
//////////////////////////////////////////////////////////////

// bool isTestMode = false;
// const char *TESTSSID = "tree house slow";
// const char *TESTPASS = "91929192";
Timer sensorReader(1000, getNewSensorMeasurement);
bool rdyForUpdate = false;

uint16_t dongleDelay = 24;     // seconds delay after dongle and densors powered up.
uint32_t totalTimeOn = 250000; // ms, total allowed time on before forced shutdown.
uint16_t LoRaDelay = 1500;     // ms, The time it will wait to receive notificaiton from server after LoRa message sent.
// uint16_t sensorWarmup = 10;    // sec, The time to warm the sensors. Set at 20s for gas, reduced to 5 otherwise
const uint16_t LoRaSendAttempts = 1;
const uint16_t LoRaConsecutiveFails = 5;
SerialLogHandler logHandler;
SensorController sensors;

///////// CHANGE THIS FOR CUSTOM SENSOR ARRANGEMENT.
//                              //  7 readings for time
//  VEML6070 uvSensor;          //  1 reading
//  BME280 weatherSensor;       //  5 readings
//  TSL2561 luxSensor;          //  4 readings
//  CCS811 gasSensor2;          //  3 readings
//  MCP9808_j tempSensor;       //  1 reading
//  HTU21 humiditySensor;       //  2 readings
//  ads1115g analogueSensors;   //  4 readings per sensor (max 4)
//  SGP30 gasSensor;            //  2 readings
//  SOIL1 soilSensor1;          //  2 readings per sensor (max 4)

// const uint16_t nStaticReadings = 15; // 14; //22 DO NOT EDIT IF NO SENSORS CHANGED! MUST MATCH SENSORCONTROLLER VALUE

String DeviceName = "";
int randomDelay;
// bool veryFirstLoad = false;

int PIR1MsgIndex = -1;
int PIR2MsgIndex = -1;
int nMessagesToSend = 0;
int nReadingsSaved = 0;
char deviceNameRet[25] = "Unknown";

EEPROMinfo ActiveEEPROMData;
int deviceNumberEEPROMAddress = 0;

// seconds within wakeup time to be concidered a wifi loop.
int secCloseToWakeup = 5; // seconds

long long installDate;

SystemSleepConfiguration config; // Global variable for sleep.
u_int16_t pirDelay = 2 * 60;     // {seconds} delay in seconds to not listen to pir.

// char JSONcode[nStaticReadings + 1][3];
// float Readings[nSaves][nStaticReadings + 1];
// retained float offlineReadings[maxOfflineSaves][nStaticReadings - nTimeReadings + 1];
// retained int offlineReadingLocation[nStaticReadings + 1];

// const short nTimeReadings = 7;
// retained uint16_t IndexOfNextOfflineReading;
retained uint16_t OfflineReadingsSavedCount;

retained uint16_t nSensorLoops = 0;
retained uint32_t sleepRemaining; // milliseconds to sleep until wifi. This is calculated at sleep.

// decimal hours last awake. Used to calculate if wakeup is PIR or timer.
retained float lastAwake;

retained bool isCalibratingGas = true;
retained long long lastCalibration;
retained bool pausedPIR = false;
retained uint32_t PIRPausedTime;

retained bool takingPhoto = false;
retained u_int16_t remainingPhotos = 365 * 2; // 600;
retained u_int16_t photosPerDay = 2;
retained uint32_t lastPhoto = 0;
retained uint32_t lastPhotoDay = 0;

u_int16_t raspberryPiActive = 6 * 60; // {seconds} time for raspberrypi to take photo
bool triggeredPhoto = false;
uint32_t raspBoot = 90; // seconds to boot raspberry pi
int raspShutdown = 15;  // seconds to shutdown raspberry pi

//////////////////////////////////////////////////////////////
//           SYSTEM TRIGGERS
//////////////////////////////////////////////////////////////
bool isSensorLoop = false;
bool isPIRLoop = false;
bool saveOffline = false;
bool waitingForName = false;
bool haveReadEEPROM = false;
bool wifiReady = false;
bool cloudReady = false;
bool connectingToCloud = false;
bool haveGotName = false;
bool haveSyncNameOnline = true;
bool haveSavedToCloudAndOffline = false;
bool currentlySendingViaLoRa = false;
retained bool loopedOfflineSaves = false;
retained bool hasAddedNewSSID = false;
retained bool isWifiOn = false;
retained bool isSensorsOn = false;
retained bool firstload = false;
retained bool LoopedPIR1 = false;
retained bool LoopedPIR2 = false;

//////////////////////////////////////////////////////////////
//            WIFI AND TIMER SETTINGS
//////////////////////////////////////////////////////////////
int cloudConnectionDuration = 5000; // ms --will turn off wifi and set isSensorLoop = false
int wifiConnectionDuration = 5000;  // ms --used in waitfor(WiFi.ready)
int wiFiConnectionAttemptMAX = 5;   // MUST be greater than 1

uint64_t timeStart = 0;
int wiFiConnectionAttempts = 0;

// WIFI CREDENTIALS for development
/*DO NOT CHANGE*/ const char *SSID1 = "3G uFi_C6B";
/*DO NOT CHANGE*/ const char *SSID2 = "3G uFi_9E3"; // installed in PowOwl
/*DO NOT CHANGE*/ const char *SSID3 = "3G uFi_C83";
/*DO NOT CHANGE*/ const char *SSID4 = "3G uFi_61B";
/*DO NOT CHANGE*/ const char *SSID5 = "3G uFi_9E0";
/*DO NOT CHANGE*/ const char *PASS = "4Qiud29-da";

//////////////////////////////////////////////////////////////
//            LoRa SETTINGS
//////////////////////////////////////////////////////////////
// Singleton instance of the radio driver
RH_RF95 rf95;
bool haveSavedToLoRa = false;
bool haveStoredSensorInfo = false;

retained uint16_t LoRaPIR1Count = 0; //, nPIR2;
retained uint16_t LoRaPIR2Count = 0; //, nPIR2;

//////////////////////////////////////////////////////////////
//            DIO AND SWITCH SETTINGS
//////////////////////////////////////////////////////////////

bool PIR1State;
bool PIR2State;
bool WkpState;

retained float PIR1[PIR1Size];
// const int PIR2SizeMod = 10;
retained float PIR2[PIR2Size];
retained uint16_t nPIR1 = 0;
retained uint16_t nPIR2 = 0;

float currentReadings[nCodesTotal - 7];
uint8_t currentReadingsJsonCode[nCodesTotal - 7];
// int nReadingsTaken = 0;
const uint16_t sRAMSize = 2500;
const uint16_t nMaxReadings = (sRAMSize - PIR1Size * 4 - PIR2Size * 4) / 5;
// retained uint16_t currentWriteIndex = 0;
retained uint16_t currentReadIndex = 0;
retained uint16_t offlineIndexEnd = 0;
retained uint16_t offlineIndexStart = 0;
uint16_t tempOfflineReadingsCount = 0;
int tempSensorCounter = 0;
retained uint8_t JSONCodeReading[nMaxReadings];
retained float offlineReadings1D[nMaxReadings];

// These are temp arrays to flag an offline/current packet was sent.
// Length is nMaxReadings in the event no sensors connected and each packet is decimal hours plus most recent one.
uint16_t tempSentReadingsLength[nMaxReadings + 1];
// bool tempSentReadingsFlag[nMaxReadings+1];

// Used for sending to LoRa.
bool haveSentArray[nMaxReadings + 1 + (PIR1Size / maxPIRCountPerMessage) + 1 + (PIR2Size / maxPIRCountPerMessage) + 1];

void getNewSensorMeasurement()
{
  if (!currentlySendingViaLoRa)
  {
    rdyForUpdate = true;
    Log.info("Taking measurement " + String(sensors.getReadingCounter()));
    /* if (tempSensorCounter != sensors.getReadingCounter())
    {
      Log.info("Taking measurement " + String(sensors.getReadingCounter()));
      tempSensorCounter = sensors.getReadingCounter();
    } */
  }
  else
    rdyForUpdate = false;
}

//////////////////////////////////////////////////////////////
//            START OF PROGRAM
// setup() runs once, when the device is first turned on.
//////////////////////////////////////////////////////////////
void setup()
{
  Serial.begin(9600);
  Time.zone(sensorsZome);
  String messageOut = setupDIO();
  delay(100);
  delay(100);
  if (isDebugMode)
  {
    while (!Serial.isConnected())
      Particle.process();
  }
  delay(500);
  Log.info("Last calibration: " + Time.format(lastCalibration));
  Log.info("Time now:         " + Time.format(Time.now()));
  Log.info("Offline count is now:   %d", OfflineReadingsSavedCount);
  Log.info("LoRa PIR1 count is now: %d", LoRaPIR1Count);
  Log.info("LoRa PIR2 count is now: %d", LoRaPIR2Count);
  Log.info("************************");
  Log.info("Starting Setup for loop.");
  Serial.println(messageOut);

  getEEPROMdata();
  Log.info("EEPROM data: %d| Name:%s | Set gBase: %d on %s | tVoC: %d | eCO2: %d |", ActiveEEPROMData.flag, ActiveEEPROMData.name, ActiveEEPROMData.setGas, Time.format(ActiveEEPROMData.gasSetDate, TIME_FORMAT_DEFAULT).c_str(), ActiveEEPROMData.TVOC_base, ActiveEEPROMData.eCO2_base);

  checkStartupFlags(); // This will record PIR and setup sensors.

  if (refreshSavedWifi)
  {
    WiFi.clearCredentials();
    delay(50);
    mycustomScan();
    // if(!foundDongle) WiFi.setCredentials(SSID2, PASS); //
    // if (!WiFi.hasCredentials() && isTestMode) WiFi.setCredentials(TESTSSID, TESTPASS); //
  }
  // WiFi.setCredentials(TESTSSID, TESTPASS);

  if (!rf95.init())
    Log.info("LoRa init failed");
  else
  {
    rf95.setFrequency(915);
    rf95.setTxPower(23, false);
    rf95.sleep();
  }

  // review sensor size.

  Log.info("Max lora message length is: " + String(rf95.maxMessageLength()));
  Log.info("Finished Setup loop.");
  Log.info("************************");
  Log.info("Last calibration: " + Time.format(lastCalibration));
  Log.info("Time now:         " + Time.format(Time.now()));
  Log.info("Offline count is now:   %d", OfflineReadingsSavedCount);
  Log.info("LoRa PIR1 count is now: %d", LoRaPIR1Count);
  Log.info("LoRa PIR2 count is now: %d", LoRaPIR2Count);

  if (saveOffline)
    Log.info("Will save offline.");
  if (isWifiOn)
    Log.info("WiFi is on.");
  if (sendViaLoRaModule)
    Log.info("LoRa module attached and will send every %d sensor loops. Currently loop %d.", nSensorLoopsPerSave, nSensorLoops);
  wd = new ApplicationWatchdog(totalTimeOn, frozenShutdownCallback, 1536);

  if (isSensorLoop)
  {
    Log.info("SensorLoop and will start sensor updates.");
    sensorReader.start();
  }
  else
    Log.info("Not sensor loop and will not start sensor updates.");

  timeStart = millis();
}

//////////////////////////////////////////////////////////////
//            START OF PROGRAM LOOP
// loop() runs over and over again, as quickly as it can execute.
//////////////////////////////////////////////////////////////
void loop()
{
  wd->checkin();
  if (millis() - timeStart > totalTimeOn)
  {
    Log.info("I am sleeepy!");
    goToSleep();
  }

  tryConnectCloud();

  hasUpdatedTime();

  updateName();

  tryToStoreSensorInfo();

  trySaveToLoRa();

  trySaveOffline();

  tryPowerDown();

  delay(500);
}

//////////////////////////////////////////////////////////////
//            Wakeup FUNCTIONS
//////////////////////////////////////////////////////////////
void checkWakeup()
{
  bool tempPIR1State = digitalRead(PIR1Pin);
  delay(10);
  bool tempPIR2State = digitalRead(PIR2Pin);
  delay(10);
  bool tempWkpState = digitalRead(WKP);
  delay(10);
  Log.info("Checking Pins again. PIR1 %s, PIR2 %s, WKP %s", tempPIR1State ? "HIGH" : "LOW", tempPIR2State ? "HIGH" : "LOW", tempWkpState ? "HIGH" : "LOW");

  checkReset();

  // update timers
  float currentTime = Time.hour() * 1.0 + Time.minute() * 1.0 / 60 + Time.second() * 1.0 / 3600;
  int secSinceLastAwake = (currentTime - lastAwake) * 3600;
  if (secSinceLastAwake < 0)
    secSinceLastAwake = secSinceLastAwake + 24 * 3600;

  //  Check PIR signals

  if (isPausingPIR && pausedPIR && (Time.now() - PIRPausedTime) > pirDelay)
  {
    Log.info("************************");
    Log.info("     Resumed PIR\n");
    Log.info("************************");
    Log.info("Finished pausing PIR after " + String((int)(Time.now() - PIRPausedTime)) + "seconds.");
    pausedPIR = false;
  }
  else if (!isPausingPIR || !pausedPIR)
  {
    if (PIR1Pin != WKP && PIR1State == HIGH)
    {
      Log.info("PIR 1 sensor high, recording decimal hours...");
      StartPIRLoop(1);
    }
    else
    {
      Log.info("PIR 1 sensor is not high");
    }

    if (PIR2Pin != WKP && PIR2State == HIGH)
    {
      Log.info("PIR 2 sensor high, recording decimal hours...");
      StartPIRLoop(2);
    }
    else
    {
      Log.info("PIR 2 sensor is not high");
    }
  }

  if (abs(secSinceLastAwake - int(sleepRemaining / 1000)) < (secCloseToWakeup)) // || abs(secSinceLastAwake - firstLoadDelay) < (secCloseToWakeup))
  {
    Log.info("Close to wakeup time, starting sensor loop: ");
    StartSensorLoop();
  }
  else
    Log.info("Not near Wakeup time...");
}

float getDecimalHr()
{
  // Log.info("Getting decimal hours for now.");
  int year = Time.year();
  long secFromEpoc = Time.now();
  int additionalSeconds = 0;
  if (year > 2020)
  {
    additionalSeconds = floor((year - 2021) / 4 + 1) * 86400;
  }
  long secStartOfYear = 1546300800 + (year - 2019) * 31536000 + additionalSeconds;
  long diffFromSoY = secFromEpoc - secStartOfYear;
  float hrs = diffFromSoY * 1.0 / 3600.0;

  return hrs;
}

float getDecimalHr(time_t timeIn)
{
  // Log.info("Calculating Dh from %d seconds", (intmax_t)time);
  int year = Time.year(timeIn);
  long secFromEpoc = (intmax_t)timeIn;
  int additionalSeconds = 0;
  if (year > 2020)
  {
    additionalSeconds = floor((year - 2021) / 4 + 1) * 86400;
  }
  long secStartOfYear = 1546300800 + (year - 2019) * 31536000 + additionalSeconds;
  // Log.info("Seconds for the start of year: " + String(secStartOfYear));
  long diffFromSoY = secFromEpoc - secStartOfYear;
  float hrs = diffFromSoY * 1.0 / 3600.0;
  return hrs;
}

time_t getTimeFromDecimal(float Dh_time)
{
  intmax_t secEpoc_Now = Time.now();
  float DhYr_Now = getDecimalHr(secEpoc_Now);
  intmax_t secYr_Now = DhYr_Now * 3600;
  intmax_t secYr_time = Dh_time * 3600;
  intmax_t secEPOC_time = secEpoc_Now - secYr_Now + secYr_time;
  time_t timeOut = secEPOC_time;

  return timeOut;
}

void StartPIRLoop(int pin)
{
  Log.info("**********************");
  Log.info("      PIR loop");
  Log.info("**********************");
  isPIRLoop = true;
  if (isPausingPIR)
    pausedPIR = true;
  float currentTime = getDecimalHr();

  if (!firstload && isControllingRaspPi)
  {

    if ((Time.now() - installDate) > 365)
    {
      photosPerDay = 2;
    }
    else
    {
      photosPerDay = remainingPhotos / (365 - ((Time.now() - installDate) * 1.0 / 86400.0));
    }
  }
  PIRPausedTime = Time.now();
  delay(10);
  if (pin == 1)
  {
    Log.info("PIR 1 detected, saving decimal hours");
    PIR1[nPIR1] = currentTime;
    nPIR1++;
    LoRaPIR1Count++;
    if (nPIR1 >= PIR1Size)
    {
      LoopedPIR1 = true;
      nPIR1 = 0;
    }
  }
  else if (pin == 2)
  {
    Log.info("PIR 2 detected, saving decimal hours");
    PIR2[nPIR2] = currentTime;
    nPIR2++;
    LoRaPIR2Count++;
    if (nPIR2 >= PIR2Size)
    {
      LoopedPIR2 = true;
      nPIR2 = 0;
    }
  }

  Log.info("**********************");
  Log.info("Taking " + String(photosPerDay) + " photos a day. " + String(remainingPhotos) + " remainging photos.");
  Log.info("Time since last photo: %d seconds. Last photoDay: %d seconds", (int)(Time.now() - lastPhoto), (int)(Time.now() - lastPhotoDay));
  Log.info("Time until next photo: %d seconds.", (int)((86400 / photosPerDay) - (Time.now() - lastPhoto)));

  if (!firstload && isControllingRaspPi && remainingPhotos > 0 && !takingPhoto && photosPerDay > 0 && (Time.now() - lastPhoto) > (86400 / photosPerDay))
  {
    Log.info("**********************");
    Log.info("      Taking Photo");
    Log.info("**********************");
    digitalWrite(raspPiPinOut, HIGH);
    delay(10);
    lastPhoto = Time.now();
    remainingPhotos--;
    if (remainingPhotos <= 0)
    {
      remainingPhotos = 0;
    }

    takingPhoto = true;
    triggeredPhoto = true;

    if ((Time.now() - lastPhotoDay) > 86400)
    {
      lastPhotoDay = Time.now();
      photosPerDay = remainingPhotos / (365 - ((Time.now() - installDate) * 1.0 / 86400.0));
    }

    if (PIR2Pin == WKP)
    {
      Log.info("Saving photo trigger on as PIR 2 decimal hours");
      PIR2[nPIR2] = currentTime;
      nPIR2++;
      LoRaPIR2Count++;
      if (nPIR2 >= PIR2Size)
      {
        LoopedPIR2 = true;
        nPIR2 = 0;
      }
    }
    Log.info("Taking " + String(photosPerDay) + " photos a day. " + String(remainingPhotos) + " remainging photos.");
  }
}

void StartSensorLoop()
{
  Log.info("**********************");
  Log.info("      Sensor loop");
  Log.info("**********************");
  nSensorLoops++;
  isSensorLoop = true;

  if (sendViaWifiModule && nSensorLoopsPerSave != 0 && nSensorLoops % nSensorLoopsPerSave == 0)
  {
    Log.info("Turning on wifi after %d loops.", nSensorLoops);
    turnOnWiFi();
    Log.info("Makeing sensor loops 0 after %d loops.", nSensorLoops);
    nSensorLoops = 0;
  }

  if (sendLoopedViaWifi && (loopedOfflineSaves || LoRaPIR1Count >= PIR1Size || LoRaPIR2Count >= PIR2Size))
  {
    turnOnWiFi();
  }

  if (!isWifiOn && !sendViaLoRaModule)
  {
    saveOffline = true;
    Log.info("No wifi or LoRa. Will save offline.");
  }

  turnOnSensors();
  setupSensors();

  // Serial.println("trying to connect to the cloud");
  // tryConnectCloud();

  Log.info("trying to get name");
  updateName();

  Log.info("Device name: " + String(deviceNameRet));
}

void tryPowerDown()
{
  if (haveSyncNameOnline && hasUpdatedTime() && (((isSensorLoop || saveOffline) && haveSavedToCloudAndOffline) || !isSensorLoop))
  {
    // resetBools();
    goToSleep();
  }
}

//////////////////////////////////////////////////////////////
//            SENSOR READING FUNCTIONS
//////////////////////////////////////////////////////////////

void tryToStoreSensorInfo()
{
  if (isSensorLoop)
  {
    if (rdyForUpdate)
    {
      // Log.info("Trying to update Sensor Info...");
      sensors.UpdateSensors();
      rdyForUpdate = false;
    }

    if (!haveStoredSensorInfo && sensors.isReadyToSend())
    {
      Log.info("Trying to Store Sensor Info...");
      StoreSensorInfo();
      delay(50);

      // Set offline save if it took too long to come online. (10 seconds before forced shutdown)
      if (millis() - timeStart > totalTimeOn * 0.9f - 10000)
      {
        Log.info("Not cloud ready in time. Saving to offline memory.");
        saveOffline = true;
      }
    }
  }
}

void StoreSensorInfo()
{
  /*
  float currentReadings[nReadings - 7];
  uint8_t currentReadingsJsonCode[nReadings - 7];
  */
  nReadingsSaved = 0;
  String tempCode = "--";
  if (!haveStoredSensorInfo)
  {
    // Log Decimal hours.
    currentReadingsJsonCode[0] = Dh_Index;
    currentReadings[0] = getDecimalHr();
    nReadingsSaved++;

    for (uint8_t i = 0; i < sensors.getLatestReadingsCount(); i++)
    {
      currentReadings[1 + i] = *(sensors.getLatestReadings() + i);
      currentReadingsJsonCode[1 + i] = *(sensors.getLatestReadingCodes() + i);
      nReadingsSaved++;
    }

    haveStoredSensorInfo = true;
    Log.info("Successful! - Stored latest sensor readings temporarily.");
  }
}

void printOfflineReadings()
{

  Serial.printf("\t[ %s:%6.1f ", sensors.JSONnames[JSONCodeReading[0]], offlineReadings1D[0]);
  for (size_t i = 1; i < nMaxReadings; i++)
  {
    if (i % 20 == 0)
    {
      Serial.printlnf("]");
      Serial.printf("\t[");

      if (offlineReadings1D[i] - (int)offlineReadings1D[i] == 0)
        Serial.printf(" %s:%6d ", sensors.JSONnames[JSONCodeReading[i]], (int)offlineReadings1D[i]);
      else
        Serial.printf(" %s:%6.1f ", sensors.JSONnames[JSONCodeReading[i]], offlineReadings1D[i]);
    }
    else
    {
      if (offlineReadings1D[i] - (int)offlineReadings1D[i] == 0)
        Serial.printf("; %s:%6d ", sensors.JSONnames[JSONCodeReading[i]], (int)offlineReadings1D[i]);
      else
        Serial.printf("; %s:%6.1f ", sensors.JSONnames[JSONCodeReading[i]], offlineReadings1D[i]);
    }
  }
  Serial.printlnf("]");
}

void printOfflineReadings2()
{
  Serial.printf("\t[ %s:%5.1f ", sensors.JSONnames[JSONCodeReading[offlineIndexStart]], offlineReadings1D[offlineIndexStart]);
  int length = offlineIndexEnd - offlineIndexStart;
  if (length < 0)
    length = nMaxReadings + length;
  // for (uint16_t i = offlineIndexStart + 1; i == offlineIndexEnd; i++)
  for (uint16_t k = 0; k < length - 1; k++)
  {
    int i = offlineIndexStart + k + 1;
    if (i >= nMaxReadings)
    {
      i = 0;
    }

    if (JSONCodeReading[i] == Dh_Index)
    {
      Serial.printlnf("]");
      Serial.printf("\t[");

      if (i == currentReadIndex)
      {
        if (offlineReadings1D[i] - (int)offlineReadings1D[i] == 0)
          Serial.printf("_%s:%5d_", sensors.JSONnames[JSONCodeReading[i]], (int)offlineReadings1D[i]);
        else
          Serial.printf("_%s:%5.1f_", sensors.JSONnames[JSONCodeReading[i]], offlineReadings1D[i]);
      }
      else if (offlineReadings1D[i] - (int)offlineReadings1D[i] == 0)
        Serial.printf(" %s:%5d ", sensors.JSONnames[JSONCodeReading[i]], (int)offlineReadings1D[i]);
      else
        Serial.printf(" %s:%5.1f ", sensors.JSONnames[JSONCodeReading[i]], offlineReadings1D[i]);
    }
    else
    {
      if (i == currentReadIndex)
      {
        if (offlineReadings1D[i] - (int)offlineReadings1D[i] == 0)
          Serial.printf(";_%s:%5d_", sensors.JSONnames[JSONCodeReading[i]], (int)offlineReadings1D[i]);
        else
          Serial.printf(";_%s:%5.1f_", sensors.JSONnames[JSONCodeReading[i]], offlineReadings1D[i]);
      }
      else if (offlineReadings1D[i] - (int)offlineReadings1D[i] == 0)
        Serial.printf("; %s:%5d ", sensors.JSONnames[JSONCodeReading[i]], (int)offlineReadings1D[i]);
      else
        Serial.printf("; %s:%5.1f ", sensors.JSONnames[JSONCodeReading[i]], offlineReadings1D[i]);
    }
  }
  Serial.printlnf("]");
}

// tempSentReadingsLength[nMessagesToSend] is populated
// nMessagesToSend is populated, and related to the sensor readings
String getFullSensorMessages()
{
  String messageOut = "";
  nMessagesToSend = 0;

  if (haveStoredSensorInfo)
  {
    if (hasSensors)
    {
      currentReadIndex = offlineIndexStart;
      // Add any offline messages until
      while (currentReadIndex != offlineIndexEnd)
      {
        // Serial.printlnf("Reading msg %d. Current read index: %d offline end index: %d. Will get more readings.", nMessagesToSend, currentReadIndex, offlineIndexEnd);
        messageOut.concat(getOfflineMessage(tempSentReadingsLength[nMessagesToSend]));
        nMessagesToSend++;
      }

      currentReadIndex = offlineIndexStart;

      if (OfflineReadingsSavedCount != nMessagesToSend)
      {
        // Log.info("%d messages to send. Expected %d offline saved", nMessagesToSend, OfflineReadingsSavedCount);
        OfflineReadingsSavedCount = nMessagesToSend;
      }

      // Log.info("Saving %d recent measurements with %d offline saved", nSaves, OfflineReadingsSavedCount)

      messageOut.concat(String("{"));
      for (uint8_t i = 0; i < nReadingsSaved; i++)
      {
        if (currentReadings[i] - (int)currentReadings[i] == 0)
        { // is int
          messageOut.concat(String("\"" + String(sensors.JSONnames[currentReadingsJsonCode[i]]) + "\":" +
                                   String(currentReadings[i], 0) + ","));
        }
        else
        {
          messageOut.concat(String("\"" + String(sensors.JSONnames[currentReadingsJsonCode[i]]) + "\":" +
                                   String(currentReadings[i], 4) + ","));
        }

        if (currentReadingsJsonCode[i] == null_index)
        {
          Serial.println("Woh!! Getting current readings for full message you went too far into null territory!");
        }
      }

      messageOut.concat(String("\"" + String(sensors.JSONnames[Yr_Index]) + "\":" + String(Time.year()) + ","));
      messageOut.concat(String("\"" + String(sensors.JSONnames[Zo_Index]) + "\":" + String(sensorsZome) + ","));
      messageOut.concat("\"Dv\":\"" + String(deviceNameRet) + "\"}");

      tempSentReadingsLength[nMessagesToSend] = nReadingsSaved;
      nMessagesToSend++;

      // Serial.println("Encoded current readings and offline recordings.\n\t"+ messageOut);
    }
    if (hasPIR)
    {
      // PIR messages.
      if (LoRaPIR1Count > 0)
      {
        // Log.info("Saving %d pir measurements with %d offline saved", LoRaPIR1Count, nPIR1);
        size_t index = (nPIR1 == 0) ? PIR1Size - 1 : nPIR1 - 1;
        uint16_t nPerLoop = maxPIRCountPerMessage;
        uint16_t nLoops;
        if (LoRaPIR1Count > PIR1Size)
          LoRaPIR1Count = PIR1Size;
        uint16_t finalLoopLength = LoRaPIR1Count % nPerLoop;
        if (finalLoopLength == 0)
          finalLoopLength = nPerLoop;
        nLoops = ceil(LoRaPIR1Count * 1.0 / nPerLoop * 1.0);
        uint16_t loopReadingsCount = 0;
        // Log.info("have %d PIR measurements to send in %d loops.", LoRaPIR1Count, nLoops);
        for (uint16_t j = 0; j < nLoops; j++)
        {
          messageOut.concat("{ \"P1\": \"");
          loopReadingsCount = (j == (nLoops - 1)) ? finalLoopLength : nPerLoop;

          // Log.info("Sending %d PIR 1 measurements loop.", loopReadingsCount);
          for (size_t i = 0; i < loopReadingsCount; i++)
          {
            messageOut.concat(String(PIR1[index], 4) + "_");
            index--;
            if (index < 0)
              index = PIR1Size - 1;
          }
          messageOut.concat("\", \"Dv\": \"" + String(deviceNameRet) + "\" }");
          nMessagesToSend++;
        }
      }
      // PIR messages.
      if (LoRaPIR2Count > 0)
      {
        // Log.info("Saving %d pir measurements with %d offline saved", LoRaPIR2Count, nPIR2);
        size_t index = nPIR2 == 0 ? PIR2Size - 1 : nPIR2 - 1;
        uint16_t nPerLoop = maxPIRCountPerMessage;
        uint16_t nLoops;
        if (LoRaPIR2Count > PIR2Size)
          LoRaPIR2Count = PIR2Size;
        uint16_t finalLoopLength = LoRaPIR2Count % nPerLoop;
        if (finalLoopLength == 0)
          finalLoopLength = nPerLoop;
        nLoops = ceil(LoRaPIR2Count * 1.0 / nPerLoop * 1.0);
        uint16_t loopReadingsCount = 0;
        // Log.info("have %d PIR measurements to send in %d loops.", LoRaPIR2Count, nLoops);
        for (uint16_t j = 0; j < nLoops; j++)
        {
          messageOut.concat("{ \"P2\": \"");
          loopReadingsCount = (j == (nLoops - 1)) ? finalLoopLength : nPerLoop;

          // Log.info("Sending %d PIR 2 measurements loop.", loopReadingsCount);
          for (size_t i = 0; i < loopReadingsCount; i++)
          {
            messageOut.concat(String(PIR2[index], 4) + "_");
            index--;
            if (index < 0)
              index = PIR2Size - 1;
          }
          messageOut.concat("\", \"Dv\": \"" + String(deviceNameRet) + "\" }");
          nMessagesToSend++;
        }
      }
    }
    // Serial.println("Message to send to LoRa");
    // Serial.println(messageOut);
  }
  return messageOut;
}

void trySaveToLoRa()
{
  //  Connections:
  //  Photon RFM96
  //  D2----------->DIO0
  //  A2----------->NSS
  //  A3----------->SCK
  //  A4----------->MISO
  //  A5----------->MOSI
  //  GND--------->GND
  //  3V3---------->3.3V IN

  if (!isWifiOn && sendViaLoRaModule &&
      (nSensorLoops >= nSensorLoopsPerSave ||
       (sendLoopedViaLoRa && (loopedOfflineSaves || LoRaPIR1Count >= PIR1Size || LoRaPIR2Count >= PIR2Size))))
  {
    if (!haveSavedToLoRa && haveStoredSensorInfo)
    {
      // printOfflineReadings();
      // Log.info("Trying to save sensor info via LoRa...");
      // Log.info("Offline count is now:      %d", OfflineReadingsSavedCount);
      // Log.info("LoRa PIR2 count is now:    %d", LoRaPIR1Count);
      // Log.info("LoRa PIR2 count is now:    %d", LoRaPIR2Count);
      // Log.info("Offline readings count is: %d", tempOfflineReadingsCount);
      // Log.info("Current offline index is:  %d", currentReadIndex);
      // Log.info("Start offline index is:    %d", offlineIndexStart);
      // Log.info("Finish offline index is:   %d", offlineIndexEnd);
      currentlySendingViaLoRa = true;
      uint16_t messageCount = 0;
      uint16_t sentCount = 0;
      String FullMessage = getFullSensorMessages();
      uint16_t failAttempts = 0;

      // Serial.println("\t" + FullMessage);

      std::fill_n(haveSentArray, nMessagesToSend, false); // fill with false flags...
      int startChar = FullMessage.indexOf('{');
      int endChar = FullMessage.indexOf('}');
      while (FullMessage.length() > 0) //|| (startChar >= 0 && endChar >= 0))
      {
        String tempMsg = "";
        // While there is more to send...
        if (startChar >= 0 && endChar >= 0)
        {
          if (failAttempts < LoRaConsecutiveFails)
          {
            tempMsg = FullMessage.substring(startChar, endChar + 1);
            // Add device name.
            tempMsg = System.deviceID() + " " + String(messageCount) + String(tempMsg);
            char dataMsg[tempMsg.length() + 1];
            tempMsg.toCharArray(dataMsg, tempMsg.length() + 1);

            uint8_t data[tempMsg.length() + 1];
            for (size_t i = 0; i < tempMsg.length() + 1; i++)
            {
              data[i] = uint8_t(dataMsg[i]);
            }
            rf95.setModeIdle();
            delay(100);
            // Send and Pool device for reply message... if time exceeds delay then send and pool again!
            for (int k = 0; k < LoRaSendAttempts; k++)
            {
              Serial.println("Sending " + String((char *)data) + " via LoRa.");
              delay(10);
              rf95.send(data, sizeof(data));
              rf95.waitPacketSent();

              rf95.available();
              long long timeNow = millis();
              while (millis() - timeNow <= LoRaDelay)
              {
                delay(10);
                waitFor(rf95.available, LoRaDelay);

                delay(10);
                if (rf95.available())
                {
                  delay(10);
                  // Should be a message for us now
                  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
                  uint8_t len = sizeof(buf);
                  if (rf95.recv(buf, &len))
                  {
                    Serial.println("Reply: " + String((char *)buf));
                    String reply = String((char *)buf);

                    int msgStart = reply.indexOf(' ');
                    if (msgStart > 0)
                    {
                      int msgEnd = reply.lastIndexOf(' ');
                      if (msgEnd == msgStart)
                        msgEnd = -1;
                      String devName = reply.substring(0, msgStart);
                      String msgNumber;
                      if (msgEnd > 0)
                        msgNumber = reply.substring(msgStart + 1, msgEnd);
                      else
                        msgNumber = reply.substring(msgStart + 1);

                      int msgCount = msgNumber.toInt();
                      if (msgNumber.equalsIgnoreCase(String(msgCount)) && devName.equalsIgnoreCase(System.deviceID()))
                      {
                        Log.info("Successful! - Sent message %d", msgCount);
                        haveSentArray[msgCount] = true;
                        sentCount++;
                        break;
                      }
                      else
                      {
                        Log.info("Fail! - Reply not correct. -%s-", System.deviceID().c_str());
                        Log.info("Fail! - Reply not correct. -%s- -%s-%d-", devName.c_str(), msgNumber.c_str(), msgCount);
                      }
                    }
                    else
                      Serial.println("No at symbol found... so not correctly received.");
                  }
                  else
                  {
                    Log.info("recv failed");
                  }
                }
                else
                {
                  Log.info("Fail! - Sensor readings not received at server.");
                }
              }
              if (haveSentArray[messageCount])
              {
                break;
              }
            }

            if (!haveSentArray[messageCount])
            {
              failAttempts++;
            }
            else
              failAttempts = 0;
          }
          FullMessage.remove(startChar, endChar + 1);
          messageCount++;
        }
        else
        {
          Log.info("Fail! - There is no brackets left but the full message has something in it...");
        }

        startChar = FullMessage.indexOf('{');
        endChar = FullMessage.indexOf('}');
      }

      if (sentCount > 0)
      {
        Log.info("Successful! - Some amount of Sensor readings sent via LoRa.");
        nSensorLoops = 0;
      }
      else
      {
        Log.info("Fail! - Sensor readings not received at server. Sent %d out of %d messages.", sentCount, messageCount);
      }
      currentlySendingViaLoRa = false;
      haveSavedToLoRa = true;
      saveOffline = true;
    }
  }
  else
  {
    Log.info("Not sending LoRa. Wifi is %d, sendLoRa %d, nSensorloop %d", isWifiOn, sendViaLoRaModule, nSensorLoops);
  }
  rf95.setModeIdle();
  delay(10);
  rf95.sleep();
  delay(10);
}

void trySaveOffline()
{
  if (isSensorLoop && haveSavedToLoRa && saveOffline && !haveSavedToCloudAndOffline)
  {
    // Serial.println("Saving offline!");
    uint16_t LoRaPIR1loops = ceil(LoRaPIR1Count * 1.0 / maxPIRCountPerMessage * 1.0);
    uint16_t LoRaPIR2loops = ceil(LoRaPIR2Count * 1.0 / maxPIRCountPerMessage * 1.0);

    Log.info("Messages to send: %d offline, %d PIR1, %d PIR2, %d current = %d total.", OfflineReadingsSavedCount, LoRaPIR1loops, LoRaPIR2loops, 1, nMessagesToSend); // magic number 1 is current reading.
    uint16_t latestReadingIndex = OfflineReadingsSavedCount;
    uint16_t PIR1IndexStart = OfflineReadingsSavedCount + 1;                 // magic number 1 is current reading.
    uint16_t PIR2IndexStart = OfflineReadingsSavedCount + 1 + LoRaPIR1loops; // magic number 1 is current reading.
    //--------------------------------------
    // Find the amount of offline readings not saved.
    if (OfflineReadingsSavedCount > 0)
    {
      // Log.info("\tChecking which offline readings sent.");
      uint16_t offRemaining = 0; //! haveSentArray[OfflineReadingsSavedCount];
      tempOfflineReadingsCount = 0;
      currentReadIndex = offlineIndexStart;
      uint16_t oldReadIndex = offlineIndexStart;

      for (size_t i = 0; i < OfflineReadingsSavedCount; i++)
      {
        // For each offline reading saved. check if it was sent.
        if (!haveSentArray[i])
        {
          // If not sent, save readings and json codes in next available slot.
          for (size_t j = 0; j < tempSentReadingsLength[i]; j++)
          {
            offlineReadings1D[currentReadIndex] = offlineReadings1D[oldReadIndex];
            JSONCodeReading[currentReadIndex] = JSONCodeReading[oldReadIndex];

            tempOfflineReadingsCount++;
            currentReadIndex++;
            if (currentReadIndex >= nMaxReadings)
              currentReadIndex = currentReadIndex - nMaxReadings;
            oldReadIndex++;
            if (oldReadIndex >= nMaxReadings)
              oldReadIndex = oldReadIndex - nMaxReadings;
          }
          offRemaining++;
        }
        else
        {
          // else if it is sent, move to next reading, passing over length of current sent readings.
          oldReadIndex += tempSentReadingsLength[i];
          if (oldReadIndex >= nMaxReadings)
            oldReadIndex = oldReadIndex - nMaxReadings;
        }
      }
      offlineIndexEnd = currentReadIndex;
      currentReadIndex = offlineIndexStart;
      // Serial.printlnf("%d/%d offline readings remaining.", offRemaining, OfflineReadingsSavedCount);
      // Serial.printlnf("New start index: %d. New end Index: %d. New offline readings count: %d.", offlineIndexStart, offlineIndexEnd, tempOfflineReadingsCount);
      OfflineReadingsSavedCount = offRemaining;
    }
    //--------------------------------------
    // if current is not saved add to offline.
    if (!haveSentArray[latestReadingIndex])
    {
      // Serial.printlnf("Latest reading not sent. Adding to offline.");
      addOfflineData(currentReadings, currentReadingsJsonCode, tempSentReadingsLength[latestReadingIndex]);
      currentReadIndex = offlineIndexStart;
    }
    else
    {
      Log.info("\tSent latest reading.");
    }

    //--------------------------------------
    // remove PIR that were saved...
    if (LoRaPIR1Count > 0)
    {
      Log.info("\tSent PIR readings.");
      // size_t index = (nPIR1 == 0) ? PIR1Size - 1 : nPIR1 - 1;
      uint16_t nPerLoop = maxPIRCountPerMessage;
      uint16_t nLoops;
      if (LoRaPIR1Count > PIR1Size)
        LoRaPIR1Count = PIR1Size;
      uint16_t finalLoopLength = LoRaPIR1Count % nPerLoop;
      if (finalLoopLength == 0)
        finalLoopLength = nPerLoop;
      nLoops = ceil(LoRaPIR1Count * 1.0 / nPerLoop * 1.0);
      // uint16_t loopReadingsCount = 0;

      if (nLoops != PIR2IndexStart - PIR1IndexStart)
        Log.info("Error! nloops and sent dont match!");

      int offRemaining = LoRaPIR1Count; //! haveSentArray[OfflineReadingsSavedCount];
      for (int i = PIR1IndexStart; i < PIR2IndexStart; i++)
      {
        if (haveSentArray[i])
        {
          if (i == (PIR2IndexStart - 1))
          {
            offRemaining = offRemaining - finalLoopLength;
          }
          else
            offRemaining = offRemaining - nPerLoop;
        }
      }
      if (offRemaining > 0)
      {
        float tempPIR[offRemaining];
        // int runningIndex = nPIR1;
        int index = (nPIR1 == 0) ? PIR1Size - 1 : nPIR1 - 1;
        int count = 0;
        for (int i = PIR1IndexStart; i < PIR2IndexStart; i++)
        {
          int amountInLoop = (i == PIR2IndexStart - 1) ? finalLoopLength : nPerLoop;

          if (!haveSentArray[i])
          {
            for (int j = 0; j < amountInLoop; j++)
            {
              tempPIR[count] = PIR1[index];
              index--;
              count++;
              if (index < 0)
              {
                index = index + PIR1Size;
              }
            }
          }
          else
          {
            index = index - amountInLoop;
            if (index < 0)
            {
              index = index + PIR1Size;
            }
          }
        }
        for (int i = 0; i < offRemaining; i++)
        {
          PIR1[i] = tempPIR[i];
        }
        nPIR1 = offRemaining;
        LoopedPIR1 = false;
        if (nPIR1 >= PIR1Size)
        {
          nPIR1 = 0;
          LoopedPIR1 = true;
        }
      }
      LoRaPIR1Count = offRemaining;
    }
    //--------------------------------------
    // remove PIR that were saved...
    if (LoRaPIR2Count > 0)
    {
      Log.info("\tSent PIR2 readings.");
      // size_t index = (nPIR2 == 0) ? PIR2Size - 1 : nPIR2 - 1;
      uint16_t nPerLoop = maxPIRCountPerMessage;
      uint16_t nLoops;
      if (LoRaPIR2Count > PIR2Size)
        LoRaPIR2Count = PIR2Size;
      uint16_t finalLoopLength = LoRaPIR2Count % nPerLoop;
      if (finalLoopLength == 0)
        finalLoopLength = nPerLoop;
      nLoops = ceil(LoRaPIR2Count * 1.0 / nPerLoop * 1.0);
      // uint16_t loopReadingsCount = 0;

      if (nLoops != nMessagesToSend - PIR2IndexStart)
        Log.info("Error! PIR2 nloops and sent dont match!");

      int offRemaining = LoRaPIR2Count; //! haveSentArray[OfflineReadingsSavedCount];
      for (int i = PIR2IndexStart; i < nMessagesToSend; i++)
      {
        if (haveSentArray[i])
        {
          if (i == (nMessagesToSend - 1))
          {
            offRemaining = offRemaining - finalLoopLength;
          }
          else
            offRemaining = offRemaining - nPerLoop;
        }
      }
      if (offRemaining > 0)
      {
        float tempPIR[offRemaining];
        // int runningIndex = nPIR1;
        int index = (nPIR2 == 0) ? PIR2Size - 1 : nPIR2 - 1;
        int count = 0;
        for (int i = PIR2IndexStart; i < nMessagesToSend; i++)
        {
          int amountInLoop = (i == nMessagesToSend - 1) ? finalLoopLength : nPerLoop;

          if (!haveSentArray[i])
          {
            for (int j = 0; j < amountInLoop; j++)
            {
              tempPIR[count] = PIR2[index];
              index--;
              count++;
              if (index < 0)
              {
                index = index + PIR2Size;
              }
            }
          }
          else
          {
            index = index - amountInLoop;
            if (index < 0)
            {
              index = index + PIR2Size;
            }
          }
        }
        for (int i = 0; i < offRemaining; i++)
        {
          PIR2[i] = tempPIR[i];
        }
        nPIR2 = offRemaining;
        LoopedPIR2 = false;
        if (nPIR2 >= PIR2Size)
        {
          nPIR2 = 0;
          LoopedPIR2 = true;
        }
      }
      else
        Log.info("No offline remaining! :D ");
      LoRaPIR2Count = offRemaining;
    }

    // Success.. all saved values are removed. should have reset if all saved in the process.

    // printOfflineReadings();
    Log.info("Successful! - Stored latest sensor readings offline.");
    Log.info("Offline count is now:      %d", OfflineReadingsSavedCount);
    Log.info("LoRa PIR2 count is now:    %d", LoRaPIR1Count);
    Log.info("LoRa PIR2 count is now:    %d", LoRaPIR2Count);
    Log.info("Offline readings count is: %d", tempOfflineReadingsCount);
    Log.info("Current offline index is:  %d", currentReadIndex);
    Log.info("Start offline index is:    %d", offlineIndexStart);
    Log.info("Finish offline index is:   %d", offlineIndexEnd);
    // Log.info("Successful! - Saved data via LoRa.");
    haveSavedToCloudAndOffline = true;
  }
}

//// This gets the text message of the next offline readings.
//   Updates "currentReadIndex" until it equals "offlineIndexEnd"
String getOfflineMessage(uint16_t &ReadingsLength)
{
  // Serial.println("Getting offline message for the full message.");
  String messageOut;
  int nReadigns = 0;
  if (currentReadIndex != offlineIndexEnd)
  {
    if (JSONCodeReading[currentReadIndex] == null_index)
    {
      Serial.println("WOH! current reading index is null!! not dh!! while getting offline message for full message.");
    }

    messageOut.concat(String("{"));
    // First one should be Decimal Hour Timestamp.
    messageOut.concat(String("\"" + String(sensors.JSONnames[JSONCodeReading[currentReadIndex]]) + "\":" +
                             String(offlineReadings1D[currentReadIndex], 4) + ","));
    // Serial.printlnf("Wrote %f to %s. index: %d, nReadigns: %d",offlineReadings1D[currentReadIndex],sensors.JSONnames[JSONCodeReading[currentReadIndex]],currentReadIndex,nReadigns);

    currentReadIndex++;
    nReadigns++;

    if (currentReadIndex >= nMaxReadings)
      currentReadIndex = 0;

    while (JSONCodeReading[currentReadIndex] != Dh_Index && JSONCodeReading[currentReadIndex] != null_index)
    {
      if (offlineReadings1D[currentReadIndex] - (int)offlineReadings1D[currentReadIndex] == 0)
      { // is int
        messageOut.concat(String("\"" + String(sensors.JSONnames[JSONCodeReading[currentReadIndex]]) + "\":" +
                                 String(offlineReadings1D[currentReadIndex], 0) + ","));
      }
      else
      {
        messageOut.concat(String("\"" + String(sensors.JSONnames[JSONCodeReading[currentReadIndex]]) + "\":" +
                                 String(offlineReadings1D[currentReadIndex], 4) + ","));
      }
      // Serial.printlnf("Wrote %f to %s. index: %d, nReadigns: %d",offlineReadings1D[currentReadIndex],sensors.JSONnames[JSONCodeReading[currentReadIndex]],currentReadIndex,nReadigns);

      currentReadIndex++;
      nReadigns++;

      if (currentReadIndex >= nMaxReadings)
        currentReadIndex = currentReadIndex - nMaxReadings;
    }
    messageOut.concat("\"Dv\":\"" + String(deviceNameRet) + "\"}");

    ReadingsLength = nReadigns;
  }
  // Serial.println("Offline message: "+ messageOut);
  return messageOut;
}

// This adds a sensor pulse to the offline data.
// It also handles overflow and removes oldest offline data if part is written over.
// Maintains the following variables:
//    offlineIndexStart - starting index of offline readings
//    offlineIndexEnd - end index of offline readings
//    tempOfflineReadingsCount - count of offline readings. should be equal to end-start or if looped max-(start-end)
void addOfflineData(float data[], uint8_t json[], uint16_t readingsCount)
{
  //-------------------------------
  //        Add decimal hours dh
  JSONCodeReading[offlineIndexEnd] = json[0]; // Dh code.
  offlineReadings1D[offlineIndexEnd] = data[0];
  IncrimentIndex();

  //-------------------------------
  //        Add each reading
  for (uint16_t i = 1; i < readingsCount; i++)
  {
    JSONCodeReading[offlineIndexEnd] = json[i]; // new value.
    offlineReadings1D[offlineIndexEnd] = data[i];
    IncrimentIndex();
  }

  //-------------------------------
  //        Add year at now
  JSONCodeReading[offlineIndexEnd] = Yr_Index; // year code.
  offlineReadings1D[offlineIndexEnd] = Time.year();
  IncrimentIndex();

  //-------------------------------
  //        Add zome, assume +10
  JSONCodeReading[offlineIndexEnd] = Zo_Index; // zone code.
  offlineReadings1D[offlineIndexEnd] = sensorsZome;
  IncrimentIndex();
  OfflineReadingsSavedCount++;
}

void IncrimentIndex()
{
  tempOfflineReadingsCount++;
  offlineIndexEnd++;
  if (offlineIndexEnd >= nMaxReadings)
    offlineIndexEnd = 0;
  if (offlineIndexEnd == offlineIndexStart)
  {
    // we have looped and need to increase the read index until next Dh.
    for (size_t i = 1; i < nMaxReadings; i++)
    {
      size_t newIndex = offlineIndexStart + i;
      if (newIndex >= nMaxReadings)
        newIndex = newIndex - nMaxReadings;
      if (JSONCodeReading[newIndex] == Dh_Index)
      {
        offlineIndexStart = newIndex;
        OfflineReadingsSavedCount--;
        break;
      }
      else
      {
        JSONCodeReading[newIndex] = null_index;
        offlineReadings1D[newIndex] = NAN;
      }
      tempOfflineReadingsCount--;
    }
  }
}

bool sendOneLoRaMessage(String _message)
{
  Log.info("Trying to save sensor info via LoRa...");
  uint16_t messageCount = 0;
  uint16_t sentCount = 0;
  int startChar = _message.indexOf('{');
  int endChar = _message.indexOf('}');
  while (_message.length() > 0) //|| (startChar >= 0 && endChar >= 0))
  {
    String tempMsg = "";
    // While there is more to send...
    if (startChar >= 0 && endChar >= 0)
    {

      tempMsg = _message.substring(startChar, endChar + 1);

      // Add device name.
      tempMsg = System.deviceID() + " " + String(messageCount) + String(tempMsg);
      char dataMsg[tempMsg.length() + 1];
      tempMsg.toCharArray(dataMsg, tempMsg.length() + 1);

      uint8_t data[tempMsg.length() + 1];
      for (size_t i = 0; i < tempMsg.length() + 1; i++)
      {
        data[i] = uint8_t(dataMsg[i]);
      }
      rf95.setModeIdle();
      delay(50);
      // Send and Pool device for reply message... if time exceeds delay then send and pool again!
      for (int k = 0; k < LoRaSendAttempts; k++)
      {
        Serial.println("Sending " + String((char *)data) + " via LoRa.");
        rf95.send(data, sizeof(data));
        rf95.waitPacketSent();

        rf95.available();
        long long timeNow = millis();
        while (millis() - timeNow <= LoRaDelay)
        {
          delay(10);
          waitFor(rf95.available, LoRaDelay);

          delay(10);
          if (rf95.available())
          {
            delay(10);
            // Should be a message for us now
            uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
            uint8_t len = sizeof(buf);
            if (rf95.recv(buf, &len))
            {
              Serial.println("Reply: " + String((char *)buf));
              String reply = String((char *)buf);

              int msgStart = reply.indexOf(' ');
              if (msgStart > 0)
              {
                int msgEnd = reply.lastIndexOf(' ');
                if (msgEnd == msgStart)
                  msgEnd = -1;
                String devName = reply.substring(0, msgStart);
                String msgNumber;
                if (msgEnd > 0)
                  msgNumber = reply.substring(msgStart + 1, msgEnd);
                else
                  msgNumber = reply.substring(msgStart + 1);

                int msgCount = msgNumber.toInt();
                if (msgNumber.equalsIgnoreCase(String(msgCount)) && devName.equalsIgnoreCase(System.deviceID()))
                {
                  Log.info("Successful! - Sent message %d", msgCount);
                  return true;
                }
                else
                {
                  Log.info("Fail! - Reply not correct. -%s-", System.deviceID().c_str());
                  Log.info("Fail! - Reply not correct. -%s- -%s-%d-", devName.c_str(), msgNumber.c_str(), msgCount);
                }
              }
              else
                Serial.println("No at symbol found... so not correctly received.");
            }
            else
            {
              Log.info("recv failed");
            }
          }
          else
          {
            Log.info("Fail! - Sensor readings not received at server.");
          }
        }
      }
      _message.remove(startChar, endChar + 1);
      messageCount++;
    }
    else
    {
      Log.info("Fail! - There is no brackets left but the full message has something in it...");
    }
    startChar = _message.indexOf('{');
    endChar = _message.indexOf('}');
  }
  if (sentCount > 0)
  {
    Log.info("Successful! - Some amount of Sensor readings sent via LoRa.");
    nSensorLoops = 0;
    return true;
  }
  else
  {
    Log.info("Fail! - Sensor readings not received at server. Sent %d out of %d messages.", sentCount, messageCount);
    return false;
  }
  rf95.setModeIdle();
  delay(10);
  rf95.sleep();
  delay(10);
}

String getBaseValueMessage(uint16_t VoCgasBase, uint16_t Co2gasBase)
{
  String messageOut = "";
  messageOut.concat(String("{"));
  // messageOut.concat(String("\"Sc\":") + String(Time.second(Time.now())) + ",");
  // messageOut.concat(String("\"Mi\":") + String(Time.minute(Time.now())) + ",");
  // messageOut.concat(String("\"Hr\":") + String(Time.hour(Time.now())) + ",");
  // messageOut.concat(String("\"Da\":") + String(Time.day(Time.now())) + ",");
  // messageOut.concat(String("\"Mo\":") + String(Time.month(Time.now())) + ",");
  messageOut.concat(String("\"Yr\":") + String(Time.year(Time.now())) + ",");
  messageOut.concat(String("\"Zo\":") + String(sensorsZome) + ",");
  messageOut.concat(String("\"Dh\":" + String(getDecimalHr(), 4) + ","));
  messageOut.concat(String("\"Bc\":" + String(Co2gasBase) + ","));
  messageOut.concat(String("\"Bv\":" + String(VoCgasBase) + ","));
  messageOut.concat("\"Dv\": \"" + String(deviceNameRet) + "\"}");
  return messageOut;
}

String getFirstMessageHeading()
{
  String messageOut = "";
  messageOut.concat(String("{"));
  messageOut.concat(String("\"Bt\":") + String("\"Save\"") + ",");     // temp
  messageOut.concat(String("\"Bh\":") + String("\"Send\"") + ",");     // humidity
  messageOut.concat(String("\"Bp\":") + String("\"Delay\"") + ",");    // pressure
  messageOut.concat(String("\"Ba\":") + String("\"Cali\"") + ",");     // alt
  messageOut.concat(String("\"Bu\":") + String("\"WiFi\"") + ",");     // abs
  messageOut.concat(String("\"Ss\":") + String("\"LoRa\"") + ",");     // co2
  messageOut.concat(String("\"Sv\":") + String("\"BUp Wifi\"") + ","); // voc
  messageOut.concat(String("\"Tl\":") + String("\"BUp LoRa\"") + ","); // PIR1
  messageOut.concat(String("\"Tv\":") + String("\"Max Save\"") + ","); // PIR2
  messageOut.concat(String("\"Ti\":") + String("\"Max PIR1\"") + ",");
  messageOut.concat(String("\"Tf\":") + String("\"Max PIR2\"") + ",");
  messageOut.concat(String("\"UV\":") + String("\"VoC\"") + ",");
  messageOut.concat(String("\"S1\":") + String("\"Co2\"") + ",");
  messageOut.concat(String("\"S2\":") + String("\"Start\"") + ",");

  /*
  messageOut.concat(String("\"P1\":") + String() + ",");
  messageOut.concat(String("\"P2\":") + String() + ",");
  messageOut.concat(String("\"Yr\":") + String() + ",");
  messageOut.concat(String("\"Mo\":") + String() + ",");
  messageOut.concat(String("\"Da\":") + String() + ",");
  messageOut.concat(String("\"Hr\":") + String() + ",");
  messageOut.concat(String("\"Mi\":") + String() + ",");
  messageOut.concat(String("\"Sc\":") + String() + ",");
  messageOut.concat(String("\"Zo\":") + String() + ",");
  messageOut.concat(String("\"Dh\":") + String() + ",");
  messageOut.concat(String("\"Bv\":") + String() + ",");
  messageOut.concat(String("\"Bc\":") + String() + ",");
  */
  messageOut.concat("\"Dv\":\"" + String(deviceNameRet) + "\"}");
  return messageOut;
}

//  BME_Temp	  BME_Hum	        BME_Pres	    BME_Alt	      BME_Abs		    SGP_CO2	    SGP_VOCs
//  Light_Lux	  Light_Visible	  Light_IR	    Light_Full	  Light_UV	  Soil_1	  Soil_2
//  PIR 1	      PIR 2	          Year	        Month	        Day	        Hour	    Minute
//  Second	    TimeZone	      DecimalHours	Base_VoC	    Base_CO2	RSSI	    Awake

String getFirstMessage()
{

  String messageOut = "";
  messageOut.concat(String("{"));
  messageOut.concat(String("\"Bt\":") + String(interval, 2) + ",");                                  // temp
  messageOut.concat(String("\"Bh\":") + String(interval * nSensorLoopsPerSave, 2) + ",");            // humidity
  messageOut.concat(String("\"Bp\":") + String(firstLoadDelay) + ",");                               // pressure
  messageOut.concat(String("\"Ba\":") + String(calibrationDuration) + ",");                          // alt
  messageOut.concat(String("\"Bu\":") + String(sendViaWifiModule ? "\"True\"" : "\"False\"") + ","); // abs
  messageOut.concat(String("\"Ss\":") + String(sendViaLoRaModule ? "\"True\"" : "\"False\"") + ","); // co2
  messageOut.concat(String("\"Sv\":") + String(sendLoopedViaWifi ? "\"True\"" : "\"False\"") + ","); // voc
  messageOut.concat(String("\"Tl\":") + String(sendLoopedViaLoRa ? "\"True\"" : "\"False\"") + ","); // PIR1
  messageOut.concat(String("\"Tv\":") + String(nMaxReadings) + ",");
  messageOut.concat(String("\"Ti\":") + String(PIR1Size) + ",");
  messageOut.concat(String("\"Tf\":") + String(PIR2Size) + ",");
  messageOut.concat(String("\"UV\":") + String(ActiveEEPROMData.TVOC_base) + ",");
  messageOut.concat(String("\"S1\":") + String(ActiveEEPROMData.eCO2_base) + ",");
  messageOut.concat(String("\"S2\":") + String(getDecimalHr(), 4) + ",");

  /*
  messageOut.concat(String("\"P1\":") + String() + ",");
  messageOut.concat(String("\"P2\":") + String() + ",");
  messageOut.concat(String("\"Yr\":") + String() + ",");
  messageOut.concat(String("\"Mo\":") + String() + ",");
  messageOut.concat(String("\"Da\":") + String() + ",");
  messageOut.concat(String("\"Hr\":") + String() + ",");
  messageOut.concat(String("\"Mi\":") + String() + ",");
  messageOut.concat(String("\"Sc\":") + String() + ",");
  messageOut.concat(String("\"Zo\":") + String() + ",");
  messageOut.concat(String("\"Dh\":") + String() + ",");
  messageOut.concat(String("\"Bv\":") + String() + ",");
  messageOut.concat(String("\"Bc\":") + String() + ",");
  */

  messageOut.concat("\"Dv\":\"" + String(deviceNameRet) + "\"}");
  return messageOut;
}

//////////////////////////////////////////////////////////////
//            SYSTEM FUNCTIONS
//////////////////////////////////////////////////////////////
bool hasUpdatedTime()
{

  if (isWifiOn || firstload)
  {
    if (Time.year() == 1999)
    {
      Log.info("Time: " + String(Time.day()) + "-" + String(Time.month()) + "-" + String(Time.year()) + " " + String(Time.hour()) + ":" + String(Time.minute()) + ":" + String(Time.second()));
      Particle.syncTime();
      Log.info("Syncing Time");
      // Log.info("Time: " + String(Time.day()) + "-" + String(Time.month()) + "-" + String(Time.year()) + " " + String(Time.hour()) + ":" + String(Time.minute()) + ":" + String(Time.second()));
      delay(100);
      return false;
    }
    else
    {
      return true;
    }
  }
  return true;
}

// Will configure sleep and return false if deep sleep and true if shallow. Raspberry pi triggers shallow.
void ConfigureSleepWithPIR(uint64_t deepSleepRemaining, uint64_t shallowSleepRemaining)
{
  if (hasPIR)
  {
    if (!firstload && isControllingRaspPi && triggeredPhoto)
    {
      Log.info("****************************************");
      Log.info("      Going to sleep for photo duration");
      Log.info("****************************************");
      triggeredPhoto = false;
      while (takingPhoto)
      {
        uint64_t raspberryPiSleep = raspberryPiActive - ((Time.now() - lastPhoto));
        Log.info("Just triggered photo. Raspberry active for " + String((int)raspberryPiSleep) + "seconds.");
        delay(50);
        SystemSleepConfiguration raspSleep;
        raspSleep.mode(SystemSleepMode::STOP)
            .gpio(raspPiPinIn, FALLING)
            .duration(raspberryPiSleep * 1000);
        System.sleep(raspSleep);
        if ((Time.now() - lastPhoto) > raspberryPiActive)
        {
          digitalWrite(raspPiPinOut, LOW);
          takingPhoto = false;
          Log.info("Finished setting raspberry pi active after " + String((int)(Time.now() - lastPhoto)) + "seconds.");
          delay(50);
        }
        else if ((Time.now() - lastPhoto) > raspBoot && digitalRead(raspPiPinIn) == 0)
        {
          if (PIR2Pin == WKP)
          {
            Log.info("Saving photo trigger on as PIR 2 decimal hours");
            delay(50);
            PIR2[nPIR2] = Time.now();
            nPIR2++;
            LoRaPIR2Count++;
            if (nPIR2 >= PIR2Size)
            {
              LoopedPIR2 = true;
              nPIR2 = 0;
            }
          }
          digitalWrite(raspPiPinOut, LOW);
          takingPhoto = false;
          Log.info("RASPBERRY PI SIGNAL RECIEVED. Finished setting raspberry pi active after " + String((int)(Time.now() - lastPhoto)) + "seconds.");
          delay(50);
          SystemSleepConfiguration raspSleep;
          raspSleep.mode(SystemSleepMode::STOP)
              .duration(raspShutdown * 1000);
          System.sleep(raspSleep);
        }
      }
    }
    /*
    Log.info("Has PIR");
    uint64_t raspberryPiSleep = UINT64_MAX; // in seconds!!!
    // check raspberry pi
    if (!firstload && isControllingRaspPi)
    {
      if (triggeredPhoto)
      {
        raspberryPiSleep = raspberryPiActive - ((Time.now() - lastPhoto));
        triggeredPhoto = false;
        Log.info("Just triggered photo. Raspberry active for " + String((int)raspberryPiSleep) + "seconds.");
      }
      else if (takingPhoto)
      {
        raspberryPiSleep = raspberryPiActive - ((Time.now() - lastPhoto));
        Log.info("Still taking photo with Raspberry active for " + String((int)(Time.now() - lastPhoto)) + "seconds and " + String((int)raspberryPiSleep) + "seconds remaining.");
        if ((Time.now() - lastPhoto) > raspberryPiActive)
        {
          digitalWrite(raspPiPinOut, LOW);
          takingPhoto = false;
          raspberryPiSleep = UINT64_MAX;
          Log.info("Finished setting raspberry pi active after " + String((int)(Time.now() - lastPhoto)) + "seconds.");
        }
      }

      if (raspberryPiSleep != UINT64_MAX)
      {
        raspberryPiSleep = raspberryPiSleep * 1000; // ms
        if (raspberryPiSleep < shallowSleepRemaining)
        {
          shallowSleepRemaining = raspberryPiSleep;
        }

        if (deepSleepRemaining != UINT64_MAX)
        {
          deepSleepRemaining = UINT64_MAX;
        }
      }
    }
    */
    if (isPausingPIR && pausedPIR)
    {
      int64_t toPauseDurationRemaining = (pirDelay - (Time.now() - PIRPausedTime));

      Log.info("     Deep sleep: " + String(deepSleepRemaining != INT64_MAX ? (int)deepSleepRemaining / 1000 : 0) +
               " seconds\n     Shallow sleep: " + String(shallowSleepRemaining != INT64_MAX ? (int)shallowSleepRemaining / 1000 : 0) +
               " seconds\n     PIR paused remaining: " + String(toPauseDurationRemaining != INT64_MAX ? (int)toPauseDurationRemaining : 0) + " seconds");

      if ((int)toPauseDurationRemaining < 1)
      {
        Log.info("************************");
        Log.info("     Resuming PIR\n");
        Log.info("     Finished pausing PIR after " + String((int)(Time.now() - PIRPausedTime)) + "seconds.");
        pausedPIR = false;
      }

      if (pausedPIR)
      {
        uint64_t toPauseDuration = toPauseDurationRemaining * 1000;
        Log.info("     PIR paused sleep: " + String(toPauseDuration != INT64_MAX ? (int)toPauseDuration / 1000 : 0) + " seconds");
        Log.info("************************");
        Log.info("     PIR Paused\n");
        if (deepSleepRemaining < shallowSleepRemaining)
        {
          if (toPauseDuration < deepSleepRemaining)
            deepSleepRemaining = toPauseDuration;

          Log.info("     Going into deep sleep with no PIR for " + String((int)deepSleepRemaining) + "milliseconds.");
          config.mode(SystemSleepMode::HIBERNATE)
              .duration(deepSleepRemaining);
        }
        else
        {
          if (toPauseDuration < shallowSleepRemaining)
            shallowSleepRemaining = toPauseDuration;

          Log.info("     Going into shallow sleep with no PIR for " + String((int)shallowSleepRemaining) + "milliseconds.");
          config.mode(SystemSleepMode::STOP)
              .duration(shallowSleepRemaining);
        }
        Log.info("************************");
        delay(50);
        return;
      }
    }

    if (deepSleepRemaining < shallowSleepRemaining)
    {
      Log.info("Going into deep sleep with PIR for " + String((int)deepSleepRemaining) + "mili seconds.");
      config.mode(SystemSleepMode::HIBERNATE)
          .gpio(WKP, RISING)
          .duration(deepSleepRemaining);
    }
    else
    {
      Log.info("Going into shallow sleep with PIR for " + String((int)shallowSleepRemaining) + "mili seconds.");
      config.mode(SystemSleepMode::STOP)
          .gpio(WKP, RISING)
          .duration(shallowSleepRemaining);
    }
  }
  else
  {
    if (deepSleepRemaining < shallowSleepRemaining)
    {
      config.mode(SystemSleepMode::HIBERNATE)
          .duration(deepSleepRemaining);
    }
    else
    {
      Log.info("Going into shallow sleep with no PIR for " + String((int)shallowSleepRemaining) + "mili seconds.");
      config.mode(SystemSleepMode::STOP)
          .duration(shallowSleepRemaining);
    }
  }
  delay(50);
}

void forcedReset()
{
  if (!firstload)
  {
    int hr = Time.hour();
    int min = Time.minute();
    int seconds = Time.second();
    // sleepRemaining = ((interval - ((hr * 60 + min) % interval) - 1) * 60 + 60 - seconds) * 1000 + randomDelay; //(interval - (min % interval) - 1) * 60 + 60 - seconds;

    sleepRemaining = (interval * 60 - ((hr * 3600 + 60 * min + seconds) % int(interval * 60))) * 1000 + randomDelay;
    Log.info("Forced Sleeping for " + String(int(sleepRemaining / 1000)) + " seconds.");
    delay(50);

    digitalWrite(sensorPin, LOW);
    isSensorsOn = false;
    delay(10);
    digitalWrite(WifiPin, LOW);
    isWifiOn = false;
    delay(10);
    if (isTestingPower)
      digitalWrite(powerPIn, LOW);
    delay(100);

    // System.sleep(SLEEP_MODE_DEEP, sleepRemaining);
    /*
    while (ConfigureSleepWithPIR(sleepRemaining,UINT64_MAX))
    {
      System.sleep(config);
      delay(100);
    }
    */
    ConfigureSleepWithPIR(sleepRemaining, UINT64_MAX);
    System.sleep(config);
  }
}

void goToSleep()
{
  if (!firstload || (firstload && haveSyncNameOnline && hasUpdatedTime()))
  {
    if (firstload)
    {
      installDate = Time.now();
      photosPerDay = remainingPhotos / 365;
      sendOneLoRaMessage(getFirstMessageHeading());
      delay(1000);
      sendOneLoRaMessage(getFirstMessage());
      randomDelay = random(-10000, 10000);
      Log.info("Loaded random delay of %d milliseconds", randomDelay);
      Log.info("taking %d photos per day with a total of %d photos.", photosPerDay, remainingPhotos);
    }

    if (isCalibratingGas)
    {
      u_int64_t calDuration = calibrationDuration;
      if (firstload)
        calDuration = FirstCalibration;

      Particle.disconnect();
      WiFi.off();
      sensors.sleep();
      digitalWrite(sensorPin, HIGH);
      delay(10);
      digitalWrite(WifiPin, LOW);
      delay(10);
      rf95.setModeIdle();
      delay(10);
      rf95.sleep();
      delay(10);
      if (isTestingPower)
        digitalWrite(powerPIn, LOW);
      delay(100);

      Log.info("Calibration Sleeping for " + String(int(calDuration)) + " seconds.");
      delay(50);
      u_int64_t startCal = Time.now();                                     // in seconds
      u_int64_t calSleepRemaining = calDuration - (Time.now() - startCal); // in sec
      while (calSleepRemaining > 0)                                        // Time.now() - startCal <= calDuration)
      {
        Log.info(String(int(calSleepRemaining)) + " seconds remaining.");
        delay(50);
        if (firstload)
        {
          Log.info(String(int(calDuration)) + " seconds remaining of calibration.");
          delay(100);
          config.mode(SystemSleepMode::STOP)
              .duration(calSleepRemaining * 1000);
        }
        else
        {
          ConfigureSleepWithPIR(UINT64_MAX, calSleepRemaining * 1000);
        }
        System.sleep(config);

        bool PIR1 = digitalRead(PIR1Pin);
        delay(10);
        bool PIR2 = digitalRead(PIR2Pin);
        delay(10);
        bool wkp = digitalRead(WKP);
        delay(10);
        Log.info("Woken up by a pin. PIR1 %s, PIR2 %s, WKP %s", PIR1 ? "HIGH" : "LOW", PIR2 ? "HIGH" : "LOW", wkp ? "HIGH" : "LOW");
        delay(10);
        //  delay(50);
        if (isPausingPIR && pausedPIR && (Time.now() - PIRPausedTime) > pirDelay)
        {
          Log.info("************************");
          Log.info("     Resumed PIR\n");
          Log.info("************************");
          Log.info("Finished pausing PIR after " + String((int)(Time.now() - PIRPausedTime)) + "seconds.");
          pausedPIR = false;
        }
        else if (!isPausingPIR || !pausedPIR)
        {
          if (PIR1Pin != WKP && PIR1)
            StartPIRLoop(1);
          if (PIR2Pin != WKP && PIR2)
            StartPIRLoop(2);
        }
        calSleepRemaining = calDuration - (Time.now() - startCal); // in s
        Log.info(String(int(calSleepRemaining)) + " seconds remaining.");
        delay(100);
      }

      delay(100);
      Log.info("Was calibrating, now getting new gas base values.");
      delay(50);
      if (sensors.hasGas())
      {
        uint16_t *gasBase = sensors.getGasBase();
        updateGasOfEEPROM(gasBase);
      }
      lastCalibration = Time.now();
      isCalibratingGas = false;
    }

    digitalWrite(sensorPin, LOW);
    isSensorsOn = false;
    delay(10);

    digitalWrite(WifiPin, LOW);
    isWifiOn = false;
    delay(10);

    if (isTestingPower)
      digitalWrite(powerPIn, LOW);
    delay(10);
    rf95.setModeIdle();
    delay(10);
    rf95.sleep();
    delay(100);
    // sendOneLoRaMessage(getTimeAwakeMessage());

    Log.info("Been awake for: " + String(millis() / 1000) + "seconds");

    if (firstload)
    {
      firstload = false;
      if (firstLoadDelay > int(calibrationDuration))
      {
        firstLoadDelay = firstLoadDelay - calibrationDuration;
      }
      else
      {
        firstLoadDelay = 1;
      }
      sleepRemaining = firstLoadDelay * 1000;
      Log.info("First load Sleeping for " + String(int(sleepRemaining / 1000)) + " seconds.");
      delay(50);

      config.mode(SystemSleepMode::HIBERNATE)
          .duration(sleepRemaining);
      System.sleep(config);
    }

    int hr = Time.hour();
    int min = Time.minute();
    int seconds = Time.second();
    // sleepRemaining = ((interval - ((hr * 60 + min) % interval) - 1) * 60 + 60 - seconds) * 1000 + randomDelay; //(interval - (min % interval) - 1) * 60 + 60 - seconds;

    sleepRemaining = (interval * 60 - ((hr * 3600 + 60 * min + seconds) % int(interval * 60))) * 1000 + randomDelay;

    lastAwake = Time.hour() * 1.0 + Time.minute() * 1.0 / 60 + Time.second() * 1.0 / 3600;

    Log.info("Usual Sleeping for " + String(int(sleepRemaining / 1000)) + " seconds.");
    delay(50);
    ConfigureSleepWithPIR(sleepRemaining, UINT64_MAX);

    /*
    while (ConfigureSleepWithPIR(sleepRemaining,UINT64_MAX))
    {
      System.sleep(config);
      delay(100);
    }
    */
    System.sleep(config);
  }
}

void checkStartupFlags()
{

  if (System.resetReason() == RESET_REASON_UNKNOWN || System.resetReason() == RESET_REASON_PANIC)
  {
    wifiReady = false;
    cloudReady = false;
    connectingToCloud = false;
    isWifiOn = false;
    saveOffline = true;
  }

  // SystemSleepResult result2 = System.sleep(config);
  SleepResult result = System.sleepResult();

  switch (result.reason())
  {
  case WAKEUP_REASON_NONE:
  {
    Log.info("Woke up for no reason.");
    if (lastCalibration == 0)
    {
      // nSensorLoops++;
      Log.info("First wakeup. Photon did not wake up from sleep.");
      firstload = true;
      haveSyncNameOnline = false;

      turnOnSensors();
      Log.info("Very First wakeup. Will do long calibration.");
      isCalibratingGas = true;

      setupSensors();

      turnOnWiFi();

      // StartSensorLoop();
    }
    else
    {
      Log.info("Sensor Loop...");
      StartSensorLoop();
    }
    break;
  }
  case WAKEUP_REASON_PIN:
  {
    Log.info("Woke up for PIN signal.");
    bool PIR1 = digitalRead(PIR1Pin);
    delay(10);

    bool PIR2 = digitalRead(PIR2Pin);
    delay(10);
    bool wkp = digitalRead(WKP);
    delay(10);
    Log.info("Woken up by a pin. PIR1 %s, PIR2 %s, WKP %s", PIR1 ? "HIGH" : "LOW", PIR2 ? "HIGH" : "LOW", wkp ? "HIGH" : "LOW");
    if (isPausingPIR && pausedPIR && (Time.now() - PIRPausedTime) > pirDelay)
    {
      Log.info("************************");
      Log.info("     Resumed PIR\n");
      Log.info("************************");
      Log.info("Finished pausing PIR after " + String((int)(Time.now() - PIRPausedTime)) + "seconds.");
      pausedPIR = false;
    }
    else if (!isPausingPIR || !pausedPIR)
    {
      if (PIR1Pin != WKP && PIR1)
        StartPIRLoop(1);
      if (PIR2Pin != WKP && PIR2)
        StartPIRLoop(2);
    }
    break;
  }
  case WAKEUP_REASON_RTC:
  {
    Log.info("Photon was woken up by the RTC (after a specified number of seconds)");
    StartSensorLoop();
    break;
  }
  case WAKEUP_REASON_PIN_OR_RTC:
  {
    Log.info("Photon was woken up by either a pin or the RTC (after a specified number of seconds)");
    checkWakeup();
    break;
  }
  }

  if (isPausingPIR && pausedPIR && (Time.now() - PIRPausedTime) > pirDelay)
  {
    Log.info("************************");
    Log.info("     Resumed PIR\n");
    Log.info("************************");
    Log.info("Finished pausing PIR after " + String((int)(Time.now() - PIRPausedTime)) + "seconds.");
    pausedPIR = false;
  }
}
//////////////////////////////////////////////////////////////
//            EEPROM AND DEVICE NAME FUNCTIONS
//////////////////////////////////////////////////////////////
bool getEEPROMdata()
{
  if (!haveReadEEPROM)
  {
    EEPROMinfo EEPROMdata;
    EEPROM.get(deviceNumberEEPROMAddress, EEPROMdata);
    if (EEPROMdata.flag != 0)
    {
      //// EEPROM was empty
      Log.info("EEPROM was empty");
      return false;
    }
    ActiveEEPROMData = EEPROMdata;
    haveReadEEPROM = true;
    // Log.info("Found EEPROM data, " + String(EEPROMdata.flag) + ": " + EEPROMdata.name);
    return true;
  }
  // Log.info("Already loaded EEPROM data. " + String(ActiveEEPROMData.flag) + "\nName: \t" + ActiveEEPROMData.name + "\nSet Gas: \t" + ActiveEEPROMData.setGas + "\nVoC: \t" + ActiveEEPROMData.TVOC_base + "\neCo2: \t" + ActiveEEPROMData.eCO2_base);
  return true;
}

void updateGasOfEEPROM(uint16_t *newBase)
{
  EEPROMinfo EEPROMdata;
  EEPROM.get(deviceNumberEEPROMAddress, EEPROMdata);

  // Log.info("Got info from EEPROM. Now checking.");
  if (EEPROMdata.flag != 0)
  {
    // EEPROM was empty -> initialize myObj
    if (haveGotName)
    {
      Log.info("Data in EEPROM is not initialized. Putting new data.");
      EEPROMdata = EEPROMinfo(DeviceName);
      if (newBase[1] != 0 || newBase[0] != 0)
      {
        EEPROMdata.TVOC_base = newBase[1];
        EEPROMdata.eCO2_base = newBase[0];
        EEPROMdata.setGas = true;
        EEPROMdata.gasSetDate = Time.now();
      }
      else
      {
        EEPROMdata.TVOC_base = newBase[1];
        EEPROMdata.eCO2_base = newBase[0];
        EEPROMdata.setGas = false;
        EEPROMdata.gasSetDate = 0;
      }
      EEPROM.put(deviceNumberEEPROMAddress, EEPROMdata);
      Log.info("Data Put...");
    }
  }
  else if (EEPROMdata.setGas)
  {
    // has set values previousely will overwrite!
    if (newBase[1] != 0 || newBase[0] != 0)
    {
      // new values are not zero. Check if they are different.
      if (newBase[0] != EEPROMdata.eCO2_base || newBase[1] != EEPROMdata.TVOC_base)
      {
        Log.info("Data exists but not the same as current base values.");
        Log.info("Base values loaded on %s in EEPROM: eCO2 %d, TVOC %d.", Time.format(EEPROMdata.gasSetDate).c_str(), EEPROMdata.eCO2_base, EEPROMdata.TVOC_base);
        Log.info("New base values in EEPROM: eCO2 %d, TVOC %d.", newBase[0], newBase[1]);

        EEPROMdata.TVOC_base = newBase[1];
        EEPROMdata.eCO2_base = newBase[0];
        EEPROMdata.setGas = true;
        EEPROMdata.gasSetDate = Time.now();
        EEPROM.put(deviceNumberEEPROMAddress, EEPROMdata);
        Log.info("Data Put...");
      }
    }
    else if (EEPROMdata.eCO2_base == 0 && EEPROMdata.TVOC_base == 0)
    {
      Log.info("New and old values are zero. setting gas to not set.");
      // new values are zero, has set gas, old values are both zero.
      EEPROMdata.setGas = false;
      EEPROMdata.gasSetDate = 0;
    }
  }
  else if (!EEPROMdata.setGas)
  {
    if (newBase[1] != 0 || newBase[0] != 0)
    {
      Log.info("Data exists but not the same as current base values.");
      Log.info("Base values loaded on %s in EEPROM: eCO2 %d, TVOC %d.", Time.format(EEPROMdata.gasSetDate).c_str(), EEPROMdata.eCO2_base, EEPROMdata.TVOC_base);
      Log.info("New base values in EEPROM: eCO2 %d, TVOC %d.", newBase[0], newBase[1]);

      EEPROMdata.TVOC_base = newBase[1];
      EEPROMdata.eCO2_base = newBase[0];
      EEPROMdata.setGas = true;
      EEPROMdata.gasSetDate = Time.now();
      EEPROM.put(deviceNumberEEPROMAddress, EEPROMdata);
      Log.info("Data Put...");
    }
  }
  // Log.info("Set new EEPROM data, " + String(EEPROMdata.flag) + ": " + EEPROMdata.name);
  EEPROM.get(deviceNumberEEPROMAddress, EEPROMdata);
  Log.info("Base values on EEPROM: eCO2 %d, TVOC %d.", newBase[0], newBase[1]);
  ActiveEEPROMData = EEPROMdata;
  sendOneLoRaMessage(getBaseValueMessage(ActiveEEPROMData.TVOC_base, ActiveEEPROMData.eCO2_base));
  haveReadEEPROM = true;
}

void setNameOfEEPROM(String newName)
{
  // Log.info("Name to save to EEPROM: " + String(newName.name));
  String Shotname;
  if (String(newName).length() > 23)
  {
    Shotname = String(newName).substring(0, 22);
    Log.info("Name shortened to: " + Shotname);
  }
  else
  {
    Shotname = String(newName);
  }

  EEPROMinfo EEPROMdata;
  EEPROM.get(deviceNumberEEPROMAddress, EEPROMdata);

  // Log.info("Got info from EEPROM. Now checking.");
  if (EEPROMdata.flag != 0)
  {
    // EEPROM was empty -> initialize myObj

    Log.info("Data in EEPROM is not initialized. Putting new data.");

    EEPROMinfo newData = EEPROMinfo(Shotname);
    EEPROM.put(deviceNumberEEPROMAddress, newData);
    Log.info("Data Put...");
  }
  else
  {
    if (String(newName) != String(EEPROMdata.name))
    {
      Log.info("Data exists but not the same as current name.");
      Log.info("Name in EEPROM: " + String(EEPROMdata.name));
      Log.info("new name      : " + String(newName));
      newName.toCharArray(EEPROMdata.name, 24);
      EEPROM.put(deviceNumberEEPROMAddress, EEPROMdata);
      Log.info("Updated name on EEPROM to " + String(EEPROMdata.name));
    }
  }
  // Log.info("Set new EEPROM data, " + String(EEPROMdata.flag) + ": " + EEPROMdata.name);
  EEPROM.get(deviceNumberEEPROMAddress, EEPROMdata);
  setNewName(EEPROMdata.name);
  Log.info("Name on sensor controller, main script and EEPROM set to " + String(EEPROMdata.name));
  ActiveEEPROMData = EEPROMdata;
  haveReadEEPROM = true;
}

void updateName()
{
  if (!haveGotName)
  {
    // Log.info("No name, Will try and get one...");
    if (getEEPROMdata())
    {
      // Log.info("Got name from EEPROM!");
      //  got EEPROM data, can update name.
      if (String(ActiveEEPROMData.name).length() == 6)
      {
        setNewName(String(ActiveEEPROMData.name));
      }
    }
  }
  if (firstload && !haveSyncNameOnline)
  {
    // Log.info("First time loaded and have not synced name.");
    if (cloudReady && !waitingForName)
    {
      // Log.info("We dont have a device name. I will register with the cloud...");
      Particle.subscribe("particle/device/name", nameHandler); //, ALL_DEVICES);
      Particle.publish("particle/device/name", PUBLIC);
      Log.info("Waiting to get device name...");
      waitingForName = true;
    }
    else if (waitingForName)
    {
      // Log.info("Still waiting for a name...");
    }
  }
  else if (!haveGotName)
  {
    // Log.info("Not a cloud loop. Checking device for name.");
    if (getEEPROMdata())
    {
      // Log.info("Got name from EEPROM!");
      //  got EEPROM data, can update name.
      setNewName(String(ActiveEEPROMData.name));
    }
    else
    {
      Log.info("Not a cloud loop and no saved name, I call thee NoName...");
      String tempName = "NoName";
      setNewName(tempName);
      // Log.info("We have a name: " + String(deviceNameRet));
    }
  }
}

void setNewName(String newName)
{
  DeviceName = String(newName);
  sensors.ComName = String(newName);
  if (newName.length() < 24)
  {
    for (size_t i = 0; i < newName.length(); i++)
    {
      deviceNameRet[i] = newName.charAt(i);
    }
    if (DeviceName.length() < 7)
    {
      for (size_t i = DeviceName.length(); i < 7; i++)
      {
        deviceNameRet[i] = '\0';
      }
    }
  }
  else
  {
    for (size_t i = 0; i < 24; i++)
    {
      deviceNameRet[i] = newName.charAt(i);
    }
  }
  haveGotName = true;
}

// This is the call back to get device name.
void nameHandler(const char *topic, const char *data)
{
  // Log.info("Call back for device name!");
  if (!haveSyncNameOnline)
  {
    String loadedNAme = String(data);
    // Log.info("received " + String(topic) + ": " + loadedNAme);
    // Log.info("Saving name...");

    // Log.info("Setting new name...: " + loadedNAme);

    // Log.info("Set name, now saving to EEPROM...");
    setNameOfEEPROM(loadedNAme);
    Log.info("We have a name in ram    : " + String(deviceNameRet));
    Log.info("We have a name in on file: " + String(DeviceName));
    Log.info("We have a name in EEPROM : " + String(ActiveEEPROMData.name));
    haveSyncNameOnline = true;
  }
}

String setupDIO()
{
  if (PIR1Pin != WKP)
    pinMode(PIR1Pin, PinMode(3)); // INPUT); //PinMode(3)=INPUT_PULLDOWN; //PinMode(3)); //I have it pulled down in the circut. Doesnt need the particle pull down.
  delay(10);

  if (PIR2Pin != WKP)
    pinMode(PIR2Pin, PinMode(3)); // INPUT); //PinMode(3)=INPUT_PULLDOWN;
  delay(10);

  pinMode(WKP, INPUT); // PinMode(3)=INPUT_PULLDOWN;
  delay(10);

  pinMode(sensorPin, OUTPUT);
  delay(10);

  if (isTestingPower)
    pinMode(powerPIn, OUTPUT);
  delay(10);

  if (isControllingRaspPi)
  {
    pinMode(raspPiPinOut, OUTPUT);
    delay(10);
    pinMode(raspPiPinIn, PinMode(2)); // INPUT_PULLDOWN = 3, INPUT_PULLUP = 2,
    delay(10);
    digitalWrite(raspPiPinOut, LOW);
    delay(10);
  }

  pinMode(WifiPin, OUTPUT);
  delay(10);

  if (PIR1Pin != WKP)
    PIR1State = digitalRead(PIR1Pin);
  delay(10);

  if (PIR2Pin != WKP)
    PIR2State = digitalRead(PIR2Pin);
  delay(10);

  WkpState = digitalRead(WKP);
  delay(10);

  if (isCalibratingGas)
  {
    digitalWrite(sensorPin, HIGH);
    isSensorsOn = true;
  }
  else
  {
    digitalWrite(sensorPin, LOW);
    isSensorsOn = false;
  }
  delay(10);

  if (isTestingPower)
    digitalWrite(powerPIn, HIGH);
  delay(10);

  digitalWrite(WifiPin, LOW);
  isWifiOn = false;
  delay(10);
  String mesOut = "Setup DIO: PIR1 " + String(PIR1State ? "HIGH " : "LOW") + ", PIR2 " + String(PIR2State ? "HIGH" : "LOW") + ", WKP " + String(WkpState ? "HIGH" : "LOW") + ", set SensorPin " + String(isSensorsOn ? "HIGH" : "LOW") + ", WifiPin " + String(isWifiOn ? "HIGH" : "LOW") + ", powerPIn HIGH";
  return mesOut;
}

void turnOnSensors()
{
  digitalWrite(sensorPin, HIGH);
  isSensorsOn = true;
  delay(1000);
}

void turnOnWiFi()
{
  // Log.info("Turning Wifi On");
  digitalWrite(WifiPin, HIGH);
  isWifiOn = true;
  delay(10);
  saveOffline = false;
  delay(dongleDelay * 1000);
}

void setupSensors()
{
  // Setting up Sensors.
  sensors.SetupSensors(); // this wakes the temp sensor...
  delay(500);
  if (sensors.hasGas())
  {
    sensors.Calibrate(); // This sets temp info into gas sensor
    delay(500);

    if (!firstload)
    {
      if ((Time.now() - lastCalibration) > calibrationFrequency)
      {
        Log.info("Wasnt calibrating, now begining to calibrate.");
        Log.info("%d seconds since last calibration, with an update of %d sec", int(Time.now() - lastCalibration), int(calibrationFrequency));
        isCalibratingGas = true;
        lastCalibration = Time.now();
      }
      else if ((haveReadEEPROM || getEEPROMdata()) && ActiveEEPROMData.setGas && (ActiveEEPROMData.eCO2_base != 0 || ActiveEEPROMData.TVOC_base != 0))
      {
        Log.info("Not calibrating and have gas values.");
        sensors.setGasBase(ActiveEEPROMData.eCO2_base, ActiveEEPROMData.TVOC_base);
      }
      Log.info("%d seconds since last calibration, with an update of %d sec", int(Time.now() - lastCalibration), int(calibrationFrequency));
      Log.info("Device is %s Calibrating.", isCalibratingGas ? "" : "not");
    }
    else if ((haveReadEEPROM || getEEPROMdata()) && ActiveEEPROMData.setGas && (ActiveEEPROMData.eCO2_base != 0 || ActiveEEPROMData.TVOC_base != 0))
    {
      Log.info("Not calibrating and have gas values.");
      sensors.setGasBase(ActiveEEPROMData.eCO2_base, ActiveEEPROMData.TVOC_base);
    }
    sensors.Calibrate(); // This sets temp info into gas sensor
    delay(500);
  }
  else
  {
    isCalibratingGas = false;
    lastCalibration = Time.now();
  }
}
//////////////////////////////////////////////////////////////
//            WIFI AND CONNECTION FUNCTIONS
//////////////////////////////////////////////////////////////
void frozenShutdownCallback()
{
  Log.info("Device is frozen. Forced shutdown...");
  forcedReset();
}

void cancelWiFiCallback()
{
  Log.info("Cancel wifi callback called...");
  if (connectingToCloud && !cloudReady)
  {
    Log.info("Took too long... will cancel Wifi and the cloud... ");
    cancelWiFi();
  }
}

void cancelWiFi()
{

  wifiReady = false;
  cloudReady = false;
  connectingToCloud = false;
  isWifiOn = false;
  saveOffline = true;
  Log.info("Took too long to connect.... Now turning off wifi.");
#if (PLATFORM_ID == PLATFORM_ARGON || PLATFORM_ID == PLATFORM_PHOTON_PRODUCTION)
  WiFi.disconnect();
  WiFi.off();
#endif
  Log.info("Turned off wifi.. now to reset bools.");
  // isSensorLoop = false;
}

void tryConnectWifi()
{
#if (PLATFORM_ID == PLATFORM_ARGON || PLATFORM_ID == PLATFORM_PHOTON_PRODUCTION)
  if (wiFiConnectionAttempts <= wiFiConnectionAttemptMAX)
  {
    WiFi.on();
    WiFi.connect();
    Log.info("Connecting WiFi...");
    if (!hasAddedNewSSID)
    {
      mycustomScan();
    }
    Log.info("Connecting to Cloud..."); ///////////////////// UPDATE
    tryConnectCloud();
  }
  else
  {
    cancelWiFi();
  }
#endif
}

void tryConnectCloud()
{
#if (PLATFORM_ID == PLATFORM_ARGON || PLATFORM_ID == PLATFORM_PHOTON_PRODUCTION)

  if (isWifiOn && !cloudReady)
  {
    if ((WiFi.ready() || WiFi.connecting())) // Mesh.ready() || Mesh.connecting())
    {
      if (WiFi.ready())
        Log.info("Wifi is ready.");
      wd->checkin();

      if (WiFi.connecting())
        Log.info("Wifi is connecting.");
      wd->checkin();

      if (waitFor(WiFi.ready, wifiConnectionDuration)) // waitFor(Mesh.ready,wifiConnectionDuration))//
      {
        Log.info("Wifi is ready. now to connect to the cloud. ");
        wd->checkin();
        wifiReady = true;
        connectingToCloud = true;
        // cloudConnectionTimer.start();
        WiFiSignal sig = WiFi.RSSI();
        Log.info("RSSI=%d", (int8_t)WiFi.RSSI());
        Log.info("WiFi signal strength: %.02f%%", sig.getStrength());
        Log.info("WiFi signal quality: %.02f%%", sig.getQuality());
        Log.info("ip address: %s", WiFi.localIP().toString().c_str());
        Log.info("subnet mask: %s", WiFi.subnetMask().toString().c_str());
        Log.info("gateway: %s", WiFi.gatewayIP().toString().c_str());

        Particle.connect();
        delay(1000);
        if (Particle.connected())
        {
          Log.info("Connected to the cloud... ");
          wd->checkin();
          cloudReady = Particle.connected();
          connectingToCloud = false;
          wiFiConnectionAttempts = 0;
          System.enableUpdates();
          Log.info("All setup, connected and ready to dance. Hazar");
        }
        else
        {
          Log.info("should be connected to cloud... ");
          wd->checkin();
          Particle.connect();
        }
      }
      else
      {
        Log.info("WiFi connection timed out, will try and connect...");
        wd->checkin();
        wiFiConnectionAttempts++;
        tryConnectWifi();
      }
      if (WiFi.connecting())
      {
        Log.info("We are still connecting to the cloud. Will try connect again.");
        wd->checkin();
        wiFiConnectionAttempts++;
        tryConnectWifi();
      }
    }
    else
    {
      Log.info("Is a cloud loop but Wifi is not enabled, will now connect.");
      wd->checkin();

      if (wiFiConnectionAttempts > 1)
        Log.info("Not connected to the cloud. will try again...");

      wd->checkin();

      wiFiConnectionAttempts++;
      wd->checkin();
      tryConnectWifi();
    }
  }
  if (isWifiOn && Particle.connected() == false)
  {
    wd->checkin();
    cloudReady = Particle.connected();
    Log.info("Connecting to the cloud... ");
    wd->checkin();
    Particle.connect();
    Log.info("should be connected to cloud... ");
    wd->checkin();
  }
#endif
}

#if (PLATFORM_ID == PLATFORM_ARGON || PLATFORM_ID == PLATFORM_PHOTON_PRODUCTION)

void mycustomScan()
{
  WiFiAccessPoint aps[20];
  WiFiAccessPoint Myap[5];
  int foundMy = WiFi.getCredentials(Myap, 5); //-1;//
  int found = WiFi.scan(aps, 20);             // 0;//
  for (int i = 0; i < found; i++)
  {
    WiFiAccessPoint &ap = aps[i];
    if (ap.ssid[0] == '3' && ap.ssid[1] == 'G')
    {
      bool haveGot = false;

      for (int j = 0; j < foundMy; j++)
      {
        if (Myap[j].ssid == ap.ssid)
        {
          haveGot = true;
          break;
        }
      }
      if (!haveGot)
      {
        Serial.print("SSID: ");
        Serial.println(ap.ssid);
        WiFi.setCredentials(ap.ssid, PASS); //
        hasAddedNewSSID = true;
      }
      else
      {
        Serial.print("I already know SSID: ");
        Serial.println(ap.ssid);
      }
    }
    else
    {

      Serial.print("Unknown SSID: ");
      Serial.println(ap.ssid);
    }
  }
}

void Add3GWifi()
{
  WiFi.scan(wifi_scan_callback); //
}

void wifi_scan_callback(WiFiAccessPoint *wap, void *data)
{
  WiFiAccessPoint &ap = *wap;
  if (ap.ssid[0] == '3' && ap.ssid[1] == 'G')
  {
    Serial.print("SSID: ");
    Serial.println(ap.ssid);
    WiFi.setCredentials(ap.ssid, PASS); //
    hasAddedNewSSID = true;
  }
  else
  {

    Serial.print("Unknown SSID: ");
    Serial.println(ap.ssid);
  }
}

#endif

void checkReset()
{
  System_Reset_Reason reset_Reason = static_cast<System_Reset_Reason>(System.resetReason());
  switch (reset_Reason)
  {
  case RESET_REASON_NONE:
  {
    Log.info("No Reason");
    break;
  }
  case RESET_REASON_UNKNOWN:
  {
    Log.info("Unspecified reason");
    break;
  }
  case RESET_REASON_PIN_RESET:
  {
    Log.info("Reset from the NRST pin");
    break;
  }
  case RESET_REASON_POWER_MANAGEMENT:
  {
    Log.info("Low-power management reset");
    break;
  }
  case RESET_REASON_POWER_DOWN:
  {
    Log.info("Power-down reset");
    break;
  }
  case RESET_REASON_POWER_BROWNOUT:
  {
    Log.info("Brownout reset");
    break;
  }
  case RESET_REASON_WATCHDOG:
  {
    Log.info("Watchdog reset");
    break;
  }
  case RESET_REASON_UPDATE:
  {
    Log.info("Successful firmware update");
    break;
  }
  case RESET_REASON_UPDATE_ERROR:
  {
    Log.info("Generic update error");
    break;
  }
  case RESET_REASON_UPDATE_TIMEOUT:
  {
    Log.info("Update timeout");
    break;
  }
  case RESET_REASON_FACTORY_RESET:
  {
    Log.info("Factory reset requested");
    break;
  }
  case RESET_REASON_SAFE_MODE:
  {
    Log.info("Safe mode requested");
    break;
  }
  case RESET_REASON_DFU_MODE:
  {
    Log.info("DFU mode requested");
    break;
  }
  case RESET_REASON_PANIC:
  {
    Log.info("System panic (additional data may contain panic code)");
    break;
  }
  case RESET_REASON_USER:
  {
    Log.info("User-requested reset");
    break;
  }
  default:
    break;
  }
}

/*

{
  "event": "{{{PARTICLE_EVENT_NAME}}}",
  "data": "{{{PARTICLE_EVENT_VALUE}}}",
  "coreid": "{{{PARTICLE_DEVICE_ID}}}",
  "published_at": "{{{PARTICLE_PUBLISHED_AT}}}",
  "Year": "{{Yr}}",
  "Decimal Hours": "{{Dh}}",
  "BME_Alt": "{{Ba}}",
  "Second": "{{Sc}}",
  "PIR 1": "{{P1}}",
  "SGP_VOCs": "{{Sv}}",
  "BME_Temp": "{{Bt}}",
  "TimeZone": "{{Zo}}",
  "Day": "{{Da}}",
  "Device": "{{Dv}}",
  "BME_Hum": "{{Bh}}",
  "BME_Pres": "{{Bp}}",
  "Minute": "{{Mi}}",
  "Hour": "{{Hr}}",
  "Month": "{{Mo}}",
  "SGP_CO2": "{{Ss}}",
  "PIR 2": "{{P2}}",
  "Client": "{{Dd}}",
  "BME_Abs": "{{Bu}}",
  "RSSI": "{{Si}}",
  "Base_CO2": "{{Bc}}",
  "Base_VoC": "{{Bv}}",
  "Awake": "{{Aw}}",
  "Light_Lux": "{{Tl}}",
  "Light_Visible": "{{Tv}}",
  "Light_IR": "{{Ti}}",
  "Light_Full": "{{Tf}}",
  "Light_UV": "{{UV}}",
  "Soil_1": "{{S1}}",
  "Soil_2": "{{S2}}"
}


*/

/*

//////////////////////////////////////////////////////////////
//            UNUSED FUNCTIONS FROM 2.0
// WHERE 1D ARRAY OF READINGS IS USED WITH MATCHING JSON CODES
//////////////////////////////////////////////////////////////
void saveOfflineData(float data[], String json[])
{
  JSONCodeReading[offlineIndexEnd] = Dh_Index; // Dh code.
  offlineReadings1D[offlineIndexEnd] = getDecimalHr();

  offlineIndexEnd++;
  if (offlineIndexEnd >= nMaxReadings)
    offlineIndexEnd = 0;
  if (offlineIndexEnd == offlineIndexStart)
  {
    // we have looped and need to increase the read index until next Dh.
    for (size_t i = 1; i < nMaxReadings; i++)
    {
      size_t newIndex = offlineIndexStart + i;
      if (newIndex >= nMaxReadings)
        newIndex = newIndex - nMaxReadings;
      if (JSONCodeReading[newIndex] == Dh_Index)
      {
        offlineIndexStart = newIndex;
        break;
      }
    }
  }

  for (size_t i = 0; i < arraySize(data); i++)
  {
    if (offlineIndexEnd == currentReadIndex)
    {
      // we have looped and need to increase the read index until next Dh.
      for (size_t i = 1; i < nMaxReadings; i++)
      {
        size_t newIndex = currentReadIndex + i;
        if (newIndex >= nMaxReadings)
          newIndex = 0;
        if (JSONCodeReading[newIndex] == Dh_Index)
        {
          currentReadIndex = newIndex;
          break;
        }
      }
    }
    JSONCodeReading[offlineIndexEnd] = getIndexInJsonArray(json[i]); // Dh code.
    offlineReadings1D[offlineIndexEnd] = data[i];

    offlineIndexEnd++;
    if (offlineIndexEnd >= nMaxReadings)
      offlineIndexEnd = 0;
    if (offlineIndexEnd == offlineIndexStart)
    {
      // we have looped and need to increase the read index until next Dh.
      for (size_t i = 1; i < nMaxReadings; i++)
      {
        size_t newIndex = offlineIndexStart + i;
        if (newIndex >= nMaxReadings)
          newIndex = newIndex - nMaxReadings;
        if (JSONCodeReading[newIndex] == Dh_Index)
        {
          offlineIndexStart = newIndex;
          break;
        }
      }
    }
  }
  JSONCodeReading[offlineIndexEnd] = Yr_Index; // Dh code.
  offlineReadings1D[offlineIndexEnd] = Time.year();

  offlineIndexEnd++;
  if (offlineIndexEnd >= nMaxReadings)
    offlineIndexEnd = 0;
  if (offlineIndexEnd == offlineIndexStart)
  {
    // we have looped and need to increase the read index until next Dh.
    for (size_t i = 1; i < nMaxReadings; i++)
    {
      size_t newIndex = offlineIndexStart + i;
      if (newIndex >= nMaxReadings)
        newIndex = newIndex - nMaxReadings;
      if (JSONCodeReading[newIndex] == Dh_Index)
      {
        offlineIndexStart = newIndex;
        break;
      }
    }
  }

  JSONCodeReading[offlineIndexEnd] = Zo_Index; // Dh code.
  offlineReadings1D[offlineIndexEnd] = 10;

  offlineIndexEnd++;
  if (offlineIndexEnd >= nMaxReadings)
    offlineIndexEnd = 0;
  if (offlineIndexEnd == offlineIndexStart)
  {
    // we have looped and need to increase the read index until next Dh.
    for (size_t i = 1; i < nMaxReadings; i++)
    {
      size_t newIndex = offlineIndexStart + i;
      if (newIndex >= nMaxReadings)
        newIndex = newIndex - nMaxReadings;
      if (JSONCodeReading[newIndex] == Dh_Index)
      {
        offlineIndexStart = newIndex;
        break;
      }
    }
  }
}



//////////////////////////////////////////////////////////////
//            UNUSED FUNCTIONS FROM 1.0
//  FUNCTION FOR CLOUD UPLOAD VIA WIFI.
//////////////////////////////////////////////////////////////
void tryToSendToCloudSensorInfo()
{
  if (isSensorLoop && !haveSavedToCloudAndOffline)
  {
    Log.info("Trying to Save Sensor Info offline or via cloud...");
    // Set offline save if it took too long to come online. (10 seconds before forced shutdown)
    if (millis() - timeStart > totalTimeOn * 0.9f - 10000)
    {
      Log.info("Not cloud ready in time. Saving to offline memory.");
      saveOffline = true;
    }

    Log.info("Havent stored info yet... will do it now.");
    // Save multiple readings
    nReadingsSaved = 0;
    for (size_t i = 0; i < nSaves; i++)
    {
      sensors.UpdateSensors();
      delay(50);
      saveSensorInfo();
      delay(50);
    }

    if (haveGotName && haveSyncNameOnline)
    {
      // Log.info("have synced name and got it. now check cloudready.");
      if ((cloudReady) && !haveSavedToCloudAndOffline)
      {
        Log.info("Cloud ready, trying to publish now.");
        // We are connected to the internet! lets dance!
        publishMyData();
        // Serial.println("Finsihed loop, " + String(nReadingsSaved) + "Now time for sleep...");
        if (nReadingsSaved >= nSaves)
        {
          nReadingsSaved = 0;
        }
        haveSavedToCloudAndOffline = true;
        Log.info("Successful! - Saved to the cloud.");
      }
    }
  }
}

// This fuction will save info to Readings variable.
// Assumed that nReadingsSaved is updated
void saveSensorInfo()
{
  String tempCode = "--";
  if (cloudReady && !haveStoredSensorInfo)
  {
    for (size_t i = 0; i < nStaticReadings; i++)
    {
      tempCode = String(sensors.getJsonCodes(i));
      JSONcode[i][0] = tempCode.charAt(0);
      JSONcode[i][1] = tempCode.charAt(1);
      JSONcode[i][2] = '\0';

      if (tempCode == "Sc")
      {
        Readings[nReadingsSaved][i] = Time.second();
      }
      else
      {
        Readings[nReadingsSaved][i] = (sensors.getLatestData())[i];
      }
    }
    Readings[nReadingsSaved][nStaticReadings] = getDecimalHr();
    nReadingsSaved++;

    haveStoredSensorInfo = true;
    Log.info("Successful! - Stored latest sensor readings temporarily.");
  }
  else if (saveOffline && haveSavedToLoRa) // This has saved SOME amount to LoRa... need to work out how much!!
  {
    int LoRaPIR1loops = ceil(LoRaPIR1Count * 1.0 / maxPIRCountPerMessage * 1.0);
    int LoRaPIR2loops = ceil(LoRaPIR2Count * 1.0 / maxPIRCountPerMessage * 1.0);
    if (IndexOfNextOfflineReading + nSaves + LoRaPIR1loops + LoRaPIR2loops != nMessagesToSend)
    {
      Log.info("ERROR -- Messages to send doesnt add up. %d offline, %d PIR1, %d PIR 2, %d current != %d total.", IndexOfNextOfflineReading, LoRaPIR1loops, LoRaPIR2loops, nSaves, nMessagesToSend);
    }
    Log.info("Successful! - sent some messages. working out which ones...");
    int latestReadingIndex = OfflineReadingsSavedCount;
    int PIR1IndexStart = OfflineReadingsSavedCount + nSaves;
    int PIR2IndexStart = OfflineReadingsSavedCount + nSaves + LoRaPIR1loops;
    //--------------------------------------
    // Find the amount of offline readings not saved.
    if (OfflineReadingsSavedCount > 0)
    {

      Log.info("\tSent some offline readings.");
      int offRemaining = 0; //! haveSentArray[OfflineReadingsSavedCount];
      for (size_t i = 0; i < OfflineReadingsSavedCount; i++)
      {
        if (!haveSentArray[i])
          offRemaining++;
      }
      float tempOffline[offRemaining][nStaticReadings - nTimeReadings + 1];

      // If has looped then start at iOffset and check each one. if it has seved move on, else copy to latestIndex.
      int latestIndex = 0; // This is where we will store a value.
      int startingOfflineindex = loopedOfflineSaves ? IndexOfNextOfflineReading : 0;
      int index;
      for (size_t i = 0; i < OfflineReadingsSavedCount; i++)
      {
        index = startingOfflineindex + i;
        if (index >= OfflineReadingsSavedCount)
          index = index - OfflineReadingsSavedCount;
        if (!haveSentArray[i])
        {
          for (size_t j = 0; j < (nStaticReadings - nTimeReadings + 1); j++)
          {
            tempOffline[latestIndex][j] = offlineReadings[index][j];
          }
          latestIndex++;
        }
      }
      for (int i = 0; i < latestIndex; i++)
      {
        for (int j = 0; j < (nStaticReadings - nTimeReadings + 1); j++)
        {
          offlineReadings[i][j] = tempOffline[i][j];
        }
      }
      if (latestIndex >= maxOfflineSaves)
      {
        OfflineReadingsSavedCount = maxOfflineSaves;
        IndexOfNextOfflineReading = 0;
        loopedOfflineSaves = true;
      }
      else
      {
        OfflineReadingsSavedCount = latestIndex;
        IndexOfNextOfflineReading = latestIndex;
        loopedOfflineSaves = false;
      }
    }
    //--------------------------------------
    // if current is not saved add to offline.
    if (!haveSentArray[latestReadingIndex])
    {
      saveLatestToOffline();
    }
    else
    {
      Log.info("\tSent latest reading.");
    }
    //--------------------------------------
    // remove PIR that were saved...
    if (LoRaPIR1Count > 0)
    {
      Log.info("\tSent PIR readings.");
      // size_t index = (nPIR1 == 0) ? PIR1Size - 1 : nPIR1 - 1;
      uint16_t nPerLoop = maxPIRCountPerMessage;
      uint16_t nLoops;
      if (LoRaPIR1Count > PIR1Size)
        LoRaPIR1Count = PIR1Size;
      uint16_t finalLoopLength = LoRaPIR1Count % nPerLoop;
      if (finalLoopLength == 0)
        finalLoopLength = nPerLoop;
      nLoops = ceil(LoRaPIR1Count * 1.0 / nPerLoop * 1.0);
      // uint16_t loopReadingsCount = 0;

      if (nLoops != PIR2IndexStart - PIR1IndexStart)
        Log.info("Error! nloops and sent dont match!");

      int offRemaining = LoRaPIR1Count; //! haveSentArray[OfflineReadingsSavedCount];
      for (int i = PIR1IndexStart; i < PIR2IndexStart; i++)
      {
        if (haveSentArray[i])
        {
          if (i == (PIR2IndexStart - 1))
          {
            offRemaining = offRemaining - finalLoopLength;
          }
          else
            offRemaining = offRemaining - nPerLoop;
        }
      }
      if (offRemaining > 0)
      {
        float tempPIR[offRemaining];
        // int runningIndex = nPIR1;
        int index = (nPIR1 == 0) ? PIR1Size - 1 : nPIR1 - 1;
        int count = 0;
        for (int i = PIR1IndexStart; i < PIR2IndexStart; i++)
        {
          int amountInLoop = (i == PIR2IndexStart - 1) ? finalLoopLength : nPerLoop;

          if (!haveSentArray[i])
          {
            for (int j = 0; j < amountInLoop; j++)
            {
              tempPIR[count] = PIR1[index];
              index--;
              count++;
              if (index < 0)
              {
                index = index + PIR1Size;
              }
            }
          }
          else
          {
            index = index - amountInLoop;
            if (index < 0)
            {
              index = index + PIR1Size;
            }
          }
        }
        for (int i = 0; i < offRemaining; i++)
        {
          PIR1[i] = tempPIR[i];
        }
        nPIR1 = offRemaining;
        LoopedPIR1 = false;
        if (nPIR1 >= PIR1Size)
        {
          nPIR1 = 0;
          LoopedPIR1 = true;
        }
      }
      LoRaPIR1Count = offRemaining;
    }
    //--------------------------------------
    // remove PIR that were saved...
    if (LoRaPIR2Count > 0)
    {
      Log.info("\tSent PIR2 readings.");
      // size_t index = (nPIR2 == 0) ? PIR2Size - 1 : nPIR2 - 1;
      uint16_t nPerLoop = maxPIRCountPerMessage;
      uint16_t nLoops;
      if (LoRaPIR2Count > PIR2Size)
        LoRaPIR2Count = PIR2Size;
      uint16_t finalLoopLength = LoRaPIR2Count % nPerLoop;
      if (finalLoopLength == 0)
        finalLoopLength = nPerLoop;
      nLoops = ceil(LoRaPIR2Count * 1.0 / nPerLoop * 1.0);
      // uint16_t loopReadingsCount = 0;

      if (nLoops != nMessagesToSend - PIR2IndexStart)
        Log.info("Error! PIR2 nloops and sent dont match!");

      int offRemaining = LoRaPIR2Count; //! haveSentArray[OfflineReadingsSavedCount];
      for (int i = PIR2IndexStart; i < nMessagesToSend; i++)
      {
        if (haveSentArray[i])
        {
          if (i == (nMessagesToSend - 1))
          {
            offRemaining = offRemaining - finalLoopLength;
          }
          else
            offRemaining = offRemaining - nPerLoop;
        }
      }
      if (offRemaining > 0)
      {
        float tempPIR[offRemaining];
        // int runningIndex = nPIR1;
        int index = (nPIR2 == 0) ? PIR2Size - 1 : nPIR2 - 1;
        int count = 0;
        for (int i = PIR2IndexStart; i < nMessagesToSend; i++)
        {
          int amountInLoop = (i == nMessagesToSend - 1) ? finalLoopLength : nPerLoop;

          if (!haveSentArray[i])
          {
            for (int j = 0; j < amountInLoop; j++)
            {
              tempPIR[count] = PIR2[index];
              index--;
              count++;
              if (index < 0)
              {
                index = index + PIR2Size;
              }
            }
          }
          else
          {
            index = index - amountInLoop;
            if (index < 0)
            {
              index = index + PIR2Size;
            }
          }
        }
        for (int i = 0; i < offRemaining; i++)
        {
          PIR2[i] = tempPIR[i];
        }
        nPIR2 = offRemaining;
        LoopedPIR2 = false;
        if (nPIR2 >= PIR2Size)
        {
          nPIR2 = 0;
          LoopedPIR2 = true;
        }
      }
      else
        Log.info("No offline remaining! :D ");
      LoRaPIR2Count = offRemaining;
    }
    // Success.. all saved values are removed. should have reset if all saved in the process.

    Log.info("Offline count is now:   %d", OfflineReadingsSavedCount);
    Log.info("LoRa PIR2 count is now: %d", LoRaPIR1Count);
    Log.info("LoRa PIR2 count is now: %d", LoRaPIR2Count);
    Log.info("Successful! - Saved data via LoRa.");
    haveSavedToCloudAndOffline = true;
    // Need to loop over saved values and remove from PIR and offline lists.
    // IndexOfNextOfflineReading = 0;
    // OfflineReadingsSavedCount = 0;
    // loopedOfflineSaves = false;
    // haveSavedToCloudAndOffline = true;
    // resetAfterSent();
  }
  else if (saveOffline && haveStoredSensorInfo && !haveSavedToLoRa)
  {
    saveLatestToOffline();
  }
  else if (saveOffline && !haveSavedToLoRa)
  {
    int count = 0;

    for (size_t i = 0; i < nStaticReadings; i++)
    {
      tempCode = String(sensors.getJsonCodes(i));
      JSONcode[i][0] = tempCode.charAt(0);
      JSONcode[i][1] = tempCode.charAt(1);
      JSONcode[i][2] = '\0';

      if (tempCode != "Sc" && tempCode != "Mi" && tempCode != "Hr" && tempCode != "Da" && tempCode != "Mo" && tempCode != "Yr" && tempCode != "Zo")
      {
        offlineReadingLocation[i] = count;
        offlineReadings[IndexOfNextOfflineReading][count] = (sensors.getLatestData())[i];
        count++;
      }
      else
      {
        offlineReadingLocation[i] = -1;
      }
      delay(10);
    }

    offlineReadings[IndexOfNextOfflineReading][nStaticReadings - nTimeReadings] = getDecimalHr();
    delay(10);
    offlineReadingLocation[nStaticReadings] = nStaticReadings - nTimeReadings;
    delay(10);

    IndexOfNextOfflineReading++;
    OfflineReadingsSavedCount++;

    if (IndexOfNextOfflineReading >= maxOfflineSaves)
    {
      IndexOfNextOfflineReading = 0;
      loopedOfflineSaves = true;
    }

    if (OfflineReadingsSavedCount > maxOfflineSaves)
    {
      OfflineReadingsSavedCount = maxOfflineSaves;
    }

    haveSavedToCloudAndOffline = true;
    Log.info("Successful! - Stored latest sensor readings offline.");
  }

  JSONcode[nStaticReadings][0] = 'D';
  JSONcode[nStaticReadings][1] = 'h';
  JSONcode[nStaticReadings][2] = '\0';
}

void saveLatestToOffline()
{
  String tempCode = "--";
  int count = 0;
  for (size_t i = 0; i < nStaticReadings; i++)
  {
    tempCode = String(sensors.getJsonCodes(i));
    JSONcode[i][0] = tempCode.charAt(0);
    JSONcode[i][1] = tempCode.charAt(1);
    JSONcode[i][2] = '\0';

    if (tempCode != "Sc" && tempCode != "Mi" && tempCode != "Hr" && tempCode != "Da" && tempCode != "Mo" && tempCode != "Yr" && tempCode != "Zo")
    {
      offlineReadingLocation[i] = count;
      offlineReadings[IndexOfNextOfflineReading][count] = Readings[nReadingsSaved][i]; //(sensors.getLatestData())[i];
      count++;
    }
    else
    {
      offlineReadingLocation[i] = -1;
    }
    // Serial.println();
    delay(10);
  }

  offlineReadings[IndexOfNextOfflineReading][nStaticReadings - nTimeReadings] = Readings[nReadingsSaved][nStaticReadings]; // getDecimalHr();
  delay(10);
  offlineReadingLocation[nStaticReadings] = nStaticReadings - nTimeReadings;
  delay(10);

  IndexOfNextOfflineReading++;
  OfflineReadingsSavedCount++;

  if (IndexOfNextOfflineReading >= maxOfflineSaves)
  {
    IndexOfNextOfflineReading = 0;
    loopedOfflineSaves = true;
  }
  if (OfflineReadingsSavedCount > maxOfflineSaves)
  {
    OfflineReadingsSavedCount = maxOfflineSaves;
  }

  haveSavedToCloudAndOffline = true;
  Log.info("Successful! - Stored latest sensor readings offline.");
}
//
void publishMyData()
{


// {
//   "Year": "{{Yr}}",
//   "Month": "{{Mo}}",
//   "Day": "{{Da}}",
//   "Hour": "{{Hr}}",
//   "Minute": "{{Mt}}",
//   "Second": "{{Sc}}",
//   "TimeZone": "{{Zo}}",
//   "BME_Temp": "{{Bt}}",
//   "BME_Pres": "{{Bp}}",
//   "BME_Alt": "{{Ba}}",
//   "BME_Hum": "{{Bh}}",
//   "SGP_CO2": "{{Ss}}",
//   "SGP_VOCs": "{{Sv}}",
//   "Device": "{{Dv}}",
//   "A1": "{{A1}}",
//   "A2": "{{A2}}",
//   "A3": "{{A3}}",
//   "A4": "{{A4}}",
//   "A5": "{{A5}}",
//   "A6": "{{A6}}",
//   "A7": "{{A7}}",
//   "A8": "{{A8}}",
//   "VEML_UV": "{{UV}}",
//   "TSL_Full": "{{Tf}}"
//   "TSL_Lux": "{{Tl}}",
//   "TSL_IR": "{{Ti}}",
//   "TSL_Vis": "{{Tv}}",
//   "PIR 1": "{{P1}}",
//   "PIR 2": "{{P2}}",
//   "Decimal Hours": "{{Dh}}"
// }

  size_t tempSavedValues = IndexOfNextOfflineReading;
  if (loopedOfflineSaves)
  {
    tempSavedValues = maxOfflineSaves;
  }

  Log.info("Uploading offline Sensor Info.");
  String tempCode = "--";
  for (size_t j = 0; j < tempSavedValues; j++)
  {
    String message = "";
    message.concat(String("{ "));
    int count = 0;
    for (size_t i = 0; i <= nStaticReadings; i++)
    {
      // message.concat(String("\"" + String(JSONcode[i]) + "\":" + String(savedReadings[j][i] * 1.0f / 1000, 3) + ", "));
      tempCode = String(JSONcode[i]);
      if (tempCode != "Sc" && tempCode != "Mi" && tempCode != "Hr" && tempCode != "Da" && tempCode != "Mo" && tempCode != "Yr" && tempCode != "Zo")
      {
        message.concat(String("\"" + tempCode + "\":" + String(offlineReadings[j][offlineReadingLocation[i]], 4) + ", "));
        // Log.info(String(i) + ":" + String(count) + ":" + String(offlineReadingLocation[i]) + " - " + tempCode + " " + String(offlineReadings[j][offlineReadingLocation[i]]));
        count++;
      }
      else
      {
        // TODO: Calculate time from decimal hours less current time.
        // Loop over readings and record date values.
        time_t readingTime = getTimeFromDecimal(offlineReadings[j][nStaticReadings - nTimeReadings]);
        // Format time, "ddd yyyy-mm-dd hh:mm:ss zzz"
        if (tempCode == "Sc")
        {
          message.concat(String("\"" + tempCode + "\":" + String(Time.second(readingTime)) + ", "));
        }
        else if (tempCode == "Mi")
        {
          message.concat(String("\"" + tempCode + "\":" + String(Time.minute(readingTime)) + ", "));
        }
        else if (tempCode == "Hr")
        {
          message.concat(String("\"" + tempCode + "\":" + String(Time.hour(readingTime)) + ", "));
        }
        else if (tempCode == "Da")
        {
          message.concat(String("\"" + tempCode + "\":" + String(Time.day(readingTime)) + ", "));
        }
        else if (tempCode == "Mo")
        {
          message.concat(String("\"" + tempCode + "\":" + String(Time.month(readingTime)) + ", "));
        }
        else if (tempCode == "Yr")
        {
          message.concat(String("\"" + tempCode + "\":" + String(Time.year(readingTime)) + ", "));
        }
        else if (tempCode == "Zo")
        {
          message.concat(String("\"" + tempCode + "\":" + String(10) + ", "));
        }
      }
    }
    // message.concat(String("\"" + String(JSONcode[nStaticReadings]) + "\":" + String(savedReadings[j][nStaticReadings] * 1.0f / 1000, 3) + ", "));
    message.concat("\"Dv\": \"" + String(deviceNameRet) + "\" }");

    Serial.println("Sent offline reading to the cloud... ");
    Log.info(message);
    Particle.publish(WebHookName, String(message), 60, PUBLIC); // WebHook to Google Sheets
    delay(1000);
    //  delete message;
  }
  IndexOfNextOfflineReading = 0;
  OfflineReadingsSavedCount = 0;

  loopedOfflineSaves = false;

  for (size_t j = 0; j < nSaves; j++)
  {
    String message = ""; //{ "--":---.---, //2019 12 30 24 60 60 10
    message.concat(String("{ "));

    for (size_t i = 0; i <= nStaticReadings; i++)
    {
      message.concat(String("\"" + String(JSONcode[i]) + "\":" + String(Readings[j][i], 4) + ", "));
    }
    // message.concat(String("\"" + String(JSONcode[nStaticReadings]) + "\":" + String(Readings[j][nStaticReadings] * 1.0f / 1000, 3) + ", "));
    message.concat("\"Dv\": \"" + String(deviceNameRet) + "\" }");

    Serial.println("Sent live reading to the cloud... ");
    Serial.println(message);
    Particle.publish(WebHookName, String(message), 60, PUBLIC); // WebHook to Google Sheets
    delay(1000);
    //  delete message;
  }

  // PIR messages.
  PublishPIRText();
  // PublishPIR1Text();
  // PublishPIR2Text();
  nReadingsSaved = 0;
}
//////////////////////////////////////////////////////////////
//            SENSOR FUNCTIONS
//////////////////////////////////////////////////////////////
void PublishPIRText()
{
  String message = "";
  // PIR messages.
  if (LoRaPIR1Count > 0)
  {
    // Log.info("Saving %d pir measurements with %d offline saved", LoRaPIR1Count, nPIR1);
    size_t index = (nPIR1 == 0) ? PIR1Size - 1 : nPIR1 - 1;
    uint16_t nPerLoop = maxPIRCountPerMessage;
    uint16_t nLoops;
    if (LoRaPIR1Count > PIR1Size)
      LoRaPIR1Count = PIR1Size;
    uint16_t finalLoopLength = LoRaPIR1Count % nPerLoop;
    if (finalLoopLength == 0)
      finalLoopLength = nPerLoop;
    nLoops = ceil(LoRaPIR1Count * 1.0 / nPerLoop * 1.0);
    uint16_t loopReadingsCount = 0;
    // Log.info("have %d PIR measurements to send in %d loops.", LoRaPIR1Count, nLoops);
    for (uint16_t j = 0; j < nLoops; j++)
    {
      message = "";
      message.concat("{ \"P1\": \"");
      loopReadingsCount = (j == (nLoops - 1)) ? finalLoopLength : nPerLoop;

      // Log.info("Sending %d PIR 1 measurements loop.", loopReadingsCount);
      for (size_t i = 0; i < loopReadingsCount; i++)
      {
        message.concat(String(PIR1[index], 4) + "_");
        index--;
        if (index < 0)
          index = PIR1Size - 1;
      }
      message.concat("\", \"Dv\": \"" + String(deviceNameRet) + "\" }");
      Log.info("Sent to the cloud... ");
      Log.info(message);
      Particle.publish(WebHookName, String(message), 60, PUBLIC); // WebHook to Google Sheets
    }

    nPIR1 = 0;
    LoRaPIR1Count = 0;
    LoopedPIR1 = false;
  }
  // PIR messages.
  if (LoRaPIR2Count > 0)
  {
    // Log.info("Saving %d pir measurements with %d offline saved", LoRaPIR2Count, nPIR2);
    size_t index = nPIR2 == 0 ? PIR2Size - 1 : nPIR2 - 1;
    uint16_t nPerLoop = maxPIRCountPerMessage;
    uint16_t nLoops;
    if (LoRaPIR2Count > PIR2Size)
      LoRaPIR2Count = PIR2Size;
    uint16_t finalLoopLength = LoRaPIR2Count % nPerLoop;
    if (finalLoopLength == 0)
      finalLoopLength = nPerLoop;
    nLoops = ceil(LoRaPIR2Count * 1.0 / nPerLoop * 1.0);
    uint16_t loopReadingsCount = 0;
    // Log.info("have %d PIR measurements to send in %d loops.", LoRaPIR2Count, nLoops);
    for (uint16_t j = 0; j < nLoops; j++)
    {
      message = "";
      message.concat("{ \"P2\": \"");
      loopReadingsCount = (j == (nLoops - 1)) ? finalLoopLength : nPerLoop;

      // Log.info("Sending %d PIR 2 measurements loop.", loopReadingsCount);
      for (size_t i = 0; i < loopReadingsCount; i++)
      {
        message.concat(String(PIR2[index], 4) + "_");
        index--;
        if (index < 0)
          index = PIR2Size - 1;
      }
      message.concat("\", \"Dv\": \"" + String(deviceNameRet) + "\" }");
      Log.info("Sent to the cloud... ");
      Log.info(message);
      Particle.publish(WebHookName, String(message), 60, PUBLIC); // WebHook to Google Sheets
    }

    nPIR2 = 0;
    LoRaPIR2Count = 0;
    LoopedPIR2 = false;
  }
}


*/