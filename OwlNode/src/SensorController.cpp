/*
 * Project SensorNode
 * Description: Main script
 * Author:  Julian Rutten - jrutten@swin.edu.au
 * Date:    11/07/2018
 */
//=======================================
//  dependency guards
#if defined(PARTICLE)
#include <Particle.h>
#if (SYSTEM_VERSION < 0x00060100)
#error requires system target 0.6.1 or above
#endif
#endif

#if defined(PARTICLE)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#if !defined(PARTICLE)
#include "Wire.h"
#endif

//======================================
//  Dependencies
#include "application.h"
#include "SensorBase.h"
#include "SensorController.h"

#include <vector>
#include <string>
#include <algorithm> // std::sort

// using namespace std;

// Add new sensors here, will automatically update and publicate them using
// overloaded functions.
void SensorController::SetupSensors()
{
  Wire.begin();

  byte error, address;
  int nDevices;

  Serial.println("Scanning...");
  nDevices = 0;
  bool ConnectedAddys[127];
  totalReadings = 0;
  for (address = 1; address < 127; address++)
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println("  !");
      ConnectedAddys[address] = true;
      nDevices++;
    }
    else if (error == 4)
    {
      Serial.print("Unknow error at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
      // ConnectedAddys[address] = false;
    }
    else
    {
      ConnectedAddys[address] = false;
    }
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");

  Serial.println("Connecting sensors...");
  int connectedDevices = 0;

  if (ConnectedAddys[BME280_addr] && weatherSensor.Connect(BME280_addr))
  {
    sensorsConnected.push_back(&weatherSensor);
    totalReadings += weatherSensor.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&weatherSensor);

  delay(10);

  if (ConnectedAddys[SGP30_addr] && gasSensor.Connect(SGP30_addr))
  {
    sensorsConnected.push_back(&gasSensor);
    totalReadings += gasSensor.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&gasSensor);
  delay(10);

  if (ConnectedAddys[TSL2561_addr] && luxSensor.Connect(TSL2561_addr))
  {
    sensorsConnected.push_back(&luxSensor);
    totalReadings += luxSensor.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&luxSensor);
  delay(10);

  if (ConnectedAddys[SOIL_1_addr] && soilSensor1.Connect(SOIL_1_addr))
  {
    sensorsConnected.push_back(&soilSensor1);
    totalReadings += soilSensor1.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&soilSensor1);
  delay(10);

  if (ConnectedAddys[SOIL_2_addr] && soilSensor2.Connect(SOIL_2_addr))
  {
    sensorsConnected.push_back(&soilSensor2);
    totalReadings += soilSensor2.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&soilSensor2);
  delay(10);

  if (ConnectedAddys[SOIL_3_addr] && soilSensor3.Connect(SOIL_3_addr))
  {
    sensorsConnected.push_back(&soilSensor3);
    totalReadings += soilSensor3.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&soilSensor3);
  delay(10);

  if (ConnectedAddys[SOIL_4_addr] && soilSensor4.Connect(SOIL_4_addr))
  {
    sensorsConnected.push_back(&soilSensor4);
    totalReadings += soilSensor4.getNumberSensors();
    connectedDevices++;
  }
  else
  {
    errorSensors.push_back(&soilSensor4);
  }
  delay(10);

  if (ConnectedAddys[VEML6070_1_addr] && ConnectedAddys[VEML6070_2_addr] && (!soilSensor3.Connect(SOIL_3_addr) && !soilSensor4.Connect(SOIL_4_addr)) && uvSensor.Connect(VEML6070_2_addr))
  {
    sensorsConnected.push_back(&uvSensor);
    totalReadings += uvSensor.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&uvSensor);
  delay(10);

  if (ConnectedAddys[ADS1115_1_addr] && analogueSensors1.Connect(ADS1115_1_addr))
  {
    sensorsConnected.push_back(&analogueSensors1);
    totalReadings += analogueSensors1.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&analogueSensors1);
  delay(10);

  if (ConnectedAddys[ADS1115_2_addr] == 1 && analogueSensors2.Connect(ADS1115_2_addr))
  {
    sensorsConnected.push_back(&analogueSensors2);
    totalReadings += analogueSensors2.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&analogueSensors2);
  delay(100);

  if (ConnectedAddys[ADS1115_3_addr] == 1 && analogueSensors3.Connect(ADS1115_3_addr))
  {
    sensorsConnected.push_back(&analogueSensors3);
    totalReadings += analogueSensors3.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&analogueSensors3);
  delay(10);

  if (ConnectedAddys[ADS1115_4_addr] == 1 && analogueSensors4.Connect(ADS1115_4_addr))
  {
    sensorsConnected.push_back(&analogueSensors4);
    totalReadings += analogueSensors4.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&analogueSensors4);
  delay(10);

  if (ConnectedAddys[CCS811_addr] && gasSensor2.Connect(CCS811_addr))
  {
    sensorsConnected.push_back(&gasSensor2);
    totalReadings += gasSensor2.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&gasSensor2);
  delay(10);

  if (ConnectedAddys[MCP9808_addr] && tempSensor.Connect(MCP9808_addr))
  {
    sensorsConnected.push_back(&tempSensor);
    totalReadings += tempSensor.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&tempSensor);
  delay(10);

  if (ConnectedAddys[HTU21D_addr] && humiditySensor.Connect(HTU21D_addr))
  {
    sensorsConnected.push_back(&humiditySensor);
    totalReadings += humiditySensor.getNumberSensors();
    connectedDevices++;
  }
  else
    errorSensors.push_back(&humiditySensor);
  delay(10);

  Serial.println(String(connectedDevices) + " Sensors found and connected correctly.");
  Serial.println(String(totalReadings) + " readings for each loop.");
}

void SensorController::calculateLatestReadigns()
{
  latestReadings.clear();
  latestReadingCodes.clear();
  std::vector<float> tempReadings;
  for (uint8_t i = 0; i < nCodesTotal; i++)
  {
    tempReadings.clear();
    for (uint8_t j = 0; j < maxSavedReadings; j++)
    {
      if (!isnan(rawReadings[i][j]))
      {
        tempReadings.push_back(rawReadings[i][j]);
      }
    }

    if (tempReadings.size() > 0)
    {
      std::sort(tempReadings.begin(), tempReadings.end());
      latestReadings.push_back(tempReadings[(int)tempReadings.size() / 2]);
      latestReadingCodes.push_back(i);
    }
  }
  readyToSend = true;
}

bool SensorController::setGasBase(uint16_t eCO2_baseline, uint16_t TVOC_baseline)
{
  if (gasSensor.isConnected() && !gasSensor.setBaseLine(eCO2_baseline, TVOC_baseline))
  {
    Serial.println("Failed to set baseline readings");
    return false;
  }
  return true;
}

uint16_t *SensorController::getGasBase()
{
  return gasSensor.returnBaseLine();
}

void SensorController::UpdateSensors()
{
  delay(10);
  if (savedReadings >= maxSavedReadings)
  {
    savedReadings = 0;
    calculateLatestReadigns();
  }

  // float rawReadings[nReadings][maxSavedReadings];
  // String rawReadingsCodes[nReadings];
  uint8_t tempIndex = 255;

  for (size_t i = 0; i < nCodesTotal; i++)
  {
    rawReadings[i][savedReadings] = NAN;
  }

  for (size_t i = 0; i < sensorsConnected.size(); i++)
  {
    // Serial.println("Updating " + sensorsConnected[i]->getName());

    sensorsConnected[i]->UpdateReading();
    for (int j = 0; j < sensorsConnected[i]->getNumberSensors(); j++)
    {
      // Serial.println((sensorsConnected[i]->getJSONCodes())[j]);
      // tempIndex = getIndexInJSONArray((sensorsConnected[i]->getJSONCodes())[j]);
      tempIndex = sensorsConnected[i]->getCodeIndexStart() + j;
      if (tempIndex != 255)
      {
        rawReadings[tempIndex][savedReadings] = (sensorsConnected[i]->getLatestValues())[j];
        // Serial.printlnf("Found in array! Index %d, %s from %s", tempIndex, JSONnames[tempIndex], sensorsConnected[i]->getName().c_str());
      }
      else
        Serial.printlnf("Code not found in array! tries to get %d, %d/%d reading from %s", tempIndex, j, sensorsConnected[i]->getNumberSensors(), sensorsConnected[i]->getName().c_str());
    }
  }

  readingCounter++;
  savedReadings++;
}

/*
for (int i = count; i < nStaticReadings; i++)
{
  JasonName[i] = "--";
  JasonNameVec.push_back(("--"));
}
*/
//  }

// if first time, setup header info.
// year, month, day, hour,min, second,
// for each connected sensor String* getJSONCodes()

//  uint32_t sensorreadingSize;
//  uint32_t timeStampReadingSize;
// sensorReadings.push_back()
// uint32_t freemem1 = System.freeMemory();
//  double latestReadingValues[totalReadings + 7];

//  latestReadingValues[0] = Time.year();
//  latestReadingValues[1] = Time.month();
//  latestReadingValues[2] = Time.day();
//  latestReadingValues[3] = Time.hour();
//  latestReadingValues[4] = Time.minute();
//  latestReadingValues[5] = Time.second();
// latestReadingValues[6] = 10;

/*
  staticReadings[savedReadings][0] = Time.year();
  staticReadings[savedReadings][1] = Time.month();
  staticReadings[savedReadings][2] = Time.day();
  staticReadings[savedReadings][3] = Time.hour();
  staticReadings[savedReadings][4] = Time.minute();
  staticReadings[savedReadings][5] = Time.second();
  staticReadings[savedReadings][6] = 10;

  count = 7;
  for (size_t i = 0; i < sensorsConnected.size(); i++)
  {
    sensorsConnected[i]->UpdateReading();
    //    Serial.println("Updating " + sensorsConnected[i]->getName());
    for (int j = 0; j < sensorsConnected[i]->getNumberSensors(); j++)
    {
      //  latestReadingValues[count] = (sensorsConnected[i]->getLatestValues())[j];

      staticReadings[savedReadings][count] = (sensorsConnected[i]->getLatestValues())[j];
      count++;
    }
  }

  // for (size_t i = count; i < nStaticReadings; i++)
  // {
  //   staticReadings[savedReadings][i] = -1;
  // }
  */
// Serial.println("Saved " + String(savedReadings) + " Readings. \tMemory free: \t" + String(System.freeMemory()));

// NOT IN USE

/*
void SensorController::CommunicateSensors()
{
  Serial.println("Memory used: " + String(System.freeMemory()) + " with " + String(savedReadings) + " number of readings.");


    // for (size_t j = 0; j < nStaticReadings; j++)
    // {
    //   Serial.print(String("\"" + JasonName[j] + "\":" + String(staticReadings[savedReadings - 1][j], 1) + ", "));
    // }
    // Serial.print("\n");
    //
  size_t msgSent = 0;

  for (size_t k = 0; k < sensorReadings.size(); k++)
  {
    //  Serial.println("Memory in upload   : " +  String(System.freeMemory()));
    String message = "";
    message.concat(String("{ "));

    int count = 7;
    for (size_t i = 0; i < sensorsConnected.size(); i++)
    {
      //  Serial.println("getting Connected sensor" + String(i));
      for (int j = 0; j < sensorsConnected[i]->getNumberSensors(); j++)
      {
        //    Serial.println("getting sensor readig" + String(j));
        message.concat(String("\"" + (sensorsConnected[i]->getJSONCodes())[j] + "\":" + String((sensorReadings[k])[count], 4) + ", "));
        count++;
      }
    }

    //                          ((*sensorReadings[k])[i])
    message.concat("\"Dv\": " + ComName + ", ");
    message.concat("\"Yr\": " + String((sensorReadings[k])[0]) + ", ");
    message.concat("\"Mo\": " + String((sensorReadings[k])[1]) + ", ");
    message.concat("\"Da\": " + String((sensorReadings[k])[2]) + ", ");
    message.concat("\"Hr\": " + String((sensorReadings[k])[3]) + ", ");
    message.concat("\"Mi\": " + String((sensorReadings[k])[4]) + ", ");
    message.concat("\"Sc\": " + String((sensorReadings[k])[5]) + ", ");
    message.concat("\"Zo\": " + String((sensorReadings[k])[6]) + " }");
    if (k == 0)
    {
      //  Serial.println(message);
    }
    if (Particle.connected)
    {
      Particle.publish("GeneralPublish", message, 60, PUBLIC); // WebHook to Google Sheets
      msgSent++;
    }
    //    Serial.println("Sending to the cloud... ");
    //  delete message;
  }

  // vector<double*>::iterator iter;

  // for (iter = sensorReadings.begin(); iter != sensorReadings.end(); iter++) {
  //     delete[] iter;
  //   }




// for (size_t i = 0; i < sensorReadings.size(); i++) {
// for (size_t j = 0; j < totalReadings + 7; j++) {

// Serial.print(String(sensorReadings[i][j],4) + " : ");
// }
// Serial.println("");
// }

// Serial.println("clearing saved data.. ");



  if (msgSent < sensorReadings.size())
  {
    for (size_t i = 0; i < msgSent; i++)
    {
      sensorReadings.erase(sensorReadings.begin());
    }
  }
  else if (msgSent == sensorReadings.size())
  {
    sensorReadings.clear();
  }
}

void SensorController::UploadSensors()
{
  if (readyToSend)
  {
    savedReadings = maxSavedReadings;
  }

  for (int i = 0; i < savedReadings; i++)
  {
    data = String();
    data.concat(String("{ "));

    // for (size_t j = 0; j < nStaticReadings; j++)
    // {
    //   data.concat(String("\"" + JasonName[j] + "\":" + String(staticReadings[i][j], 4) + ", "));
    //   staticReadings[i][j] = -1;
    //}

    data.concat("\"Dv\": " + ComName + "} ");
    if (Particle.connected)
    {
      Particle.publish("Burnley", data, 60, PRIVATE); // WebHook to Google Sheets
      // Particle.publish(ComName,data, 60, PRIVATE);

      unsigned long now = millis();
      while ((millis() - now) <= 1000)
      {
      }
    }
  }
  savedReadings = 0;
  readyToSend = false;
  //  Serial.println("Memory used: " + String(System.freeMemory()) + "before upload.");
  data = String();
  sensorReadings.clear();
}

*/

// sentMessage = true;
//   Serial.println("Pushed to the cloud. " + String(System.freeMemory()));
//   WiFi.off();

//  }
/*
String data = String("{ ");
for (size_t i = 0; i < sensorsConnected.size(); i++) {
data.concat(sensorsConnected[i]->getJSONData());
}
data.concat("\"Da\": " + Time.format(time, TIME_FORMAT_ISO8601_FULL) + " }");
Serial.println(data);
*/
// char payload[255];
// snprintf(payload, sizeof(payload), "{ \"Ct\": %.2f, \"Cc\": %.2f, \"Cv\": %.2f, \"Tl\": %.2f,\"Ht\": %.2f, \"Hh\": %.2f }",
// CCStemp.getAverage(), CCSco2.getAverage(), CCSvoc.getAverage(), TSLlux.getAverage(), HTUtemp.getAverage(), HTUhumid.getAverage());

// Particle.publish("SensorReading", SensorInfo);
//   Particle.publish("SensorReading", payload);

/*

JSON Format


{
"CCStemp": "{{Ct}}",
"CCSco2": "{{Cc}}",
"CCSvoc": "{{Cv}}",
"TSLlux": "{{Tl}}",
"TSLir": "{{Ti}}",
"TSLvis": "{{Tv}}",
"HTUtemp": "{{Ht}}",
"HTUhum": "{{Hh}}",
"BMEtemp": "{{Bt}}",
"BMEpres": "{{Bp}}",
"BMEhum": "{{Ba}}",
"MCPtemp": "{{Mt}}",
"VEMuv": "{{UV}}",
"Date": "{{Da}}",
"device_id": "{{{PARTICLE_DEVICE_ID}}}",
"published_at": "{{{PARTICLE_PUBLISHED_AT}}}"
}



*/
